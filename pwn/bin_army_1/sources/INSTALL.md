# Bin Army 1

## Installation

```bash
./main.py setup
```

## Nettoyage

```bash
./main.py clean
```

## Test

```bash
./main.py test
```
