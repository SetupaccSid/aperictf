#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc,char** argv)
{
	printf("Running Test.class in Java Sandbox ...\n");
	setreuid(0,0);
	setregid(0,0);
	char* args[] = {"/opt/jdk1.8.0_60/bin/java","-Djava.security.manager","-Xmx8G","Test",NULL};
	execve("/opt/jdk1.8.0_60/bin/java",args,NULL);
}
