# Pwn-Run-See

## Étape 1

Reynholm Industries est une entreprise mystérieuse qui ne souhaite pas révéler son activité au public.

Cependant, suite à une investigation, vous avez découvert un service de gestion de ticket exposé sur l'un de leurs serveurs.

Étudiez son fonctionnement et trouvez un moyen de prendre la main sur leur serveur.

```bash
nc pwn-run-see.aperictf.fr 31337
```

<u>**Fichiers&nbsp;:**</u>
- [chall](files/chall) - md5sum: 87ede5b3bbbba3405426376334edbe7b
- [chall.c](files/chall.c) - md5sum: 0536b8b742cd50e8315aca02a097f927
- [libc.so.6](files/libc.so.6) - md5sum: b4e619245a924b949d8ba780e1a07cee

## Étape 2

Le service de gestion de ticket semble tourner dans un conteneur, par chance, vous avez trouvé son fichier de configuration sur Internet.

Trouvez un moyen de prendre la main sur le serveur principal et découvrez-en plus sur Reynholm Industries.

<u>**Fichier&nbsp;:**</u> [docker-compose.yml](files/docker-compose.yml) - md5sum: 4d206e5f5cf1a7b93c43f672625c2eef
