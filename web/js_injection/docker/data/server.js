const fs           = require('fs')
const crypto       = require('crypto');
const _            = require('lodash');
const express      = require('express')
const bodyParser   = require('body-parser');
const cookieParser = require('cookie-parser');

const Twig = require('twig'), // Twig module
      twig = Twig.twig;       // Render function
const app  = express()

app.use(cookieParser());
app.use(bodyParser());

app.use(express.static('public'));
app.use(express.static('js'));
app.use(express.static('css'));

var config = {
    host : process.env.APP_HOST || '127.0.0.1',
    port : process.env.APP_PORT || '3000'
}

var infos = {
    js   : "js/script.js",
    css  : "css/style.css",
}


var list_notes = [
    {
        message : "Welcome to my note editor",
        user : "Areizen",
        date : Date.now()
    }
]

app.get('/',function(req,res){
    index = fs.readFileSync(__dirname + '/index.twig').toString('utf8')
    
    index = index.replace("--TITLE--",infos.title)    
    index = index.replace("--SCRIPT--",infos.js)
    index = index.replace("--CSS--",infos.css) 

    var template = twig({
        data: index
    });

    res.send(template.render())
})

app.post('/createNote', function(req,res){
        current_note = req.body
        console.log("Added Note : " + current_note.message)
        if(current_note.message != undefined 
            && current_note.user != undefined){
                list_notes.push(_.merge({date: Date.now()},current_note))
                res.send("Note added")
            }
        else{ res.send("Note not in the good format")}
})

app.get('/getNotes', function(req,res){
    res.json(_.groupBy(list_notes,x => x.user))
})

app.get('/getFlag',function(req, res){
    sha256_handler = crypto.createHash('sha256')
    sha256_handler.update(_.capitalize(req.cookies.password), 'utf8')
    hash = sha256_handler.digest().toString('hex')
    admin_hash = (config.admin_hash == undefined)? "2f37fb5343e824cc3274cfefcc8d4104b3083aec97051e0e9550f8f9aa3aa319" : config.admin_hash
    if(hash == admin_hash){
        res.sendFile(__dirname +"/flag1.txt")
    }else{
        res.send("Not the good pass bro :/")
    }

})

app.listen(config.port,function(){
    console.log("app started")
})