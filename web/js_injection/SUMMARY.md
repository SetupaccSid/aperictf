# JS Injection - Part 1

Allez hop hop hop, on passe admin !

<u>**Fichiers&nbsp;:**</u> [source.zip](source.zip) - md5sum: e53021363acf9e683b76360c8f4474f3

<u>URI&nbsp;:</u> [https://jsinject.aperictf.fr](https://jsinject.aperictf.fr)

# JS Injection - Part 2

Qu'est-ce qui pourrait être mieux que des droits Admin sur l'application ? :D

<u>**Fichiers&nbsp;:**</u> [source.zip](source.zip) - md5sum: e53021363acf9e683b76360c8f4474f3

<u>URI&nbsp;:</u> [https://jsinject.aperictf.fr](https://jsinject.aperictf.fr)
