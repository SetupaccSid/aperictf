import requests
import urllib3

from os import environ
from flask import (Flask, render_template, request, redirect, url_for)
from flask_httpauth import HTTPTokenAuth
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from urllib.parse import urlparse

# Configuration.
APP_HOST = environ.get('APP_HOST', '127.0.0.1')
APP_PORT = int(environ.get('APP_PORT', 8080))
LOGIN_URL = '/login'

users = ['admin']

# Flask.
app = Flask(__name__)
config = {
    'DEBUG': False,
    'SECRET_KEY': environ.get('APP_SECRET_KEY', '12345'),
}
app.config.from_mapping(config)
app.url_map.strict_slashes = False

# Authentication.
token_serializer = Serializer(app.config['SECRET_KEY'])
auth = HTTPTokenAuth('Bearer')

# Admin cookie.
admin_token = token_serializer.dumps({'admin': True}).decode('utf-8')
# app.logger.info(admin_token)

# Requests.
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

@app.route('/', methods=['GET'])
def home():
    return render_template('index.html'), 200

@app.route('/', methods=['POST'])
def scan():
    url = request.form['url']
    app.logger.info('Scanning URL: {:s}'.format(url))

    oracle_session = requests.session()
    if urlparse(url).netloc == 'oracle.aperictf.fr':
        app.logger.debug('Scanning internal URL!')
        oracle_session.headers.update({'Authorization': 'Bearer {:s}'.format(admin_token)})

    oracle_session.get(url, verify=False)

    # TODO: implement security check!

    return render_template('index.html', messages=['This tool is under development, please come back later!']), 200

@auth.verify_token
def verify_token(token):
    try:
        data = token_serializer.loads(token)
    except:
        # we really don't care what is the error here, any tokens that do not
        # pass validation are rejected
        result = False
    else:
        if 'admin' in data and data['admin'] == True:
            result = True
        else:
            result = False

    return result

@auth.error_handler
def auth_error():
    return 'Admin account required!', 401, {'WWW-Authenticate': 'Bearer realm="Authentication Required"'}

@app.route('/admin', methods=['GET'])
@auth.login_required
def admin():
    return 'APRK{N3v3R_3ver_7ruSt_Y0Ur_F1l73rs!}'

if __name__ == '__main__':
    app.run(host=APP_HOST, port=APP_PORT)
