# Oracle

Reynholm Industries emploie de nombreux stagiaires. L'un d'eux s'est lancé dans un projet ambitieux : développer une solution d'analyse automatique de pages WEB.

Baptisé Oracle, cet outil a pour vocation d'analyser les URLs qui lui sont envoyées afin d'y déceler des vulnérabilités ou fuites d'informations confidentielles.

Toujours en cours de développement, son encadrant (vous) souhaite vérifier la viabilité du projet et sa sécurité avant d'envisager le proposer à ses clients.

Fort heureusement, ce stagiaire vous fournit l'ensemble du projet vous permettant d'en analyser son code et sa configuration.

<u>URI&nbsp;:</u> [https://oracle.aperictf.fr/](https://oracle.aperictf.fr/)

<u>**Fichiers&nbsp;:**</u> [sources.tar.gz](files/sources.tar.gz) - md5sum: 6135733bddf27e5e29267ca07070fc9a
