<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" encoding="utf-8" indent="no"/>
    <xsl:template match="arts">
        <style>
            ::-moz-selection { background: rgba(51, 70, 209, 0.3); }
            ::selection { background: rgba(51, 70, 209, 0.3); }
            *{
                color: #3b3b3b;
            }
            html{
                background: white;
                color: #3b3b3b;
            }
            #loaddiv{
                background: #1d1d1d;
            }
            a,a:link,a:visited{
                color: #6573db;
            }
            #titre{
                color: #3b3b3b;
                border-top: 2px solid black;
                border-bottom: 2px solid black;
            }
            h2{
                font-size: 24px;
                color: #6c6c6c;
            }
            #topright div{
                background: #6573db;
            }
            #topright:hover div{
                background: black;
            }
            #networks div{
                background: #999999;
            }
            #networks div i{
                color: #e1e1e1;
            }
            #networks div:hover i{
                color: white;
            }
            #menuleft a div{
                background: #3e3e3e;
                border: 3px solid #dbdbdb;
            }
            #menuleft a:hover div:not(.active){
                background: #4a4a4a;
                border-color: #4a4a4a;
            }
            .nextsection .square{
                background: #7e7e7e;
            }
            .nextsection:hover .square{
                background: black;
            }
            .nextsection .nexttext{
                color: #9a9a9a;
            }
            .nextsection:hover .nexttext{
                color: #212121;
            }
            #menuright{
                background: white;
            }
            #menuright a{
                color: #3b3b3b;
            }
            #menuright a:hover{
                color: #6573db;
            }
            .contact h2{
                color: #3b3b3b;
                text-shadow: rgba(0, 0, 0, 0.2) 0px 1px 2px;
            }
            .needhelp{
                color: #6c6c6c;
            }
            input, textarea{
                background: #ebebeb;
                color: #3b3b3b;
            }
            input:hover,input:focus, textarea:hover, textarea:focus{
                background: #adadad;
            }
            #send_but{
                color:#4c4c4c;
                border: 1px solid #c5c5c5;
            }
            #send_but i{
                transition: all ease-in-out 0ms;
            }
            #send_but:hover i{
                color: #6573db;
            }
            #send_but:hover{
                border: 1px solid #6573db;
            }
            #formcontact div h3{
                text-shadow: rgba(0, 0, 0, 0.2) 0px 1px 2px
            }
            #formcontact > div:last-child{
                color: #3b3b3b;
            }
            .bgimage{
                background-image: none !important;
            }
        </style>
        <div id="fixed">
            <div id="loaddiv"></div>
            <div id="topleft">JS Art</div>
            <div id="topright"><div></div><div></div><div></div></div>
            <div id="menuright">
                <div>
                    <a href="#home">Home</a>
                    <a href="#art1">Arts</a>
                    <a href="#contact">Contact</a>
                    <a href="#theme">Theme</a>
                </div>
                <div id="exit"><i class="icon fa fa-times"></i></div>
            </div>
            <div id="bottomleft">
                <i class="icon fa fa-phone-square" style="margin-right: 5px;"></i> + 01 234 567 89
                <i class="icon fa fa-envelope-open" style="margin-right: 5px;margin-left:10px;"></i> js.art@aperictf.fr
            </div>
            <div id="bottomright">Chall by <a href="https://twitter.com/zeecka_" target="blank">Zeecka</a> for Aperi'CTF</div>
            <div id="networks">
                <a href="https://twitter.com/AperiKube" target="blank"><div><i class="icon fa fa-twitter"></i></div></a>
                <a href="https://www.linkedin.com/company/12988696" target="blank"><div><i class="icon fa fa-linkedin"></i></div></a>
                <a href="mailto:aperikube@protonmail.com"><div><i class="icon fa fa-envelope-open"></i></div></a>
            </div>
            <div id="menuleft">
                <a href="#home"><div class="active"></div></a>
                <a href="#art1"><div></div></a>
                <a href="#art2"><div></div></a>
                <a href="#art3"><div></div></a>
                <a href="#art4"><div></div></a>
                <a href="#art5"><div></div></a>
                <a href="#art6"><div></div></a>
                <a href="#art7"><div></div></a>
                <a href="#art8"><div></div></a>
                <a href="#oc"><div></div></a>
                <a href="#contact"><div></div></a>
                <a href="#theme"><div></div></a>
            </div>
        </div>
        <div id="fullpage">
            <div class="section">
                <h1 id="titre">JS Art</h1>
                <h2 id="soustitre">Welcome to our JavaScript art gallery, the greatest art of our world.</h2>
                <div class="nextsection"><div class="square"></div><div class="nexttext">Show Arts</div></div>
            </div>
            <xsl:apply-templates/>
            <div class="section quoteline">
                Original contents are from <a href="https://www.dwitter.net/" target="_blank">dwitter.net</a>
                <div class="nextsection"><div class="square"></div><div class="nexttext">Themes</div></div>
            </div>
            <div class="section contact">
                <h2>Contact Us</h2>
                <div class="needhelp">Need more informations? Don't Forget to Contact With Us</div>
                <div id="formcontact">
                    <div>
                        <div>Name</div>
                        <input type="text" id="inp_name"/>
                        <div>Email</div>
                        <input type="email" id="inp_mail"/>
                        <div>Message</div>
                        <textarea id="inp_msg"></textarea>
                        <div id="send_but"><i class="icon fa fa-envelope" style="margin-right: 5px"></i>SEND</div>
                        <div id="ressendmail"></div>
                    </div>
                    <div>
                        <h3>Address</h3>
                        27 Rue du Maréchal Leclerc, VANNES<br/>
                        15 Place des Lices, 35000 RENNES<br/><br/>
                        <h3>Phone</h3>
                        + 01 234 567 89<br/><br/>
                        <h3>Email</h3>
                        js.art@aperictf.fr
                        <div class="bgimage"></div>
                    </div>
                </div>
            </div>
            <div class="section quoteline">You may have a look to the <a href="/?dark">Dark Theme</a>.</div>
        </div>
    </xsl:template>

    <!-- For Each Dwitter -->

    <xsl:template match="dwitter">
        <div class="section">
            <div class="dwitter">
            <h2><xsl:value-of select="title"/></h2>
            <xsl:element name="a">
                <xsl:attribute name="href">https://www.dwitter.net/d/<xsl:value-of select="id"/></xsl:attribute>
                <xsl:attribute name="target">blank</xsl:attribute>
                <div class="iframe-wrapper">
                    <div class="nofocusiframe"></div>
                    <xsl:element name="div">
                        <xsl:attribute name="id">iframe-container-<xsl:value-of select="id"/></xsl:attribute>
                    </xsl:element>
                </div>
            </xsl:element>
                <script>
                    var code = "<xsl:value-of select="code"/>";
                    replaceIframe(code,"iframe-container-<xsl:value-of select="id"/>");
                </script>
            </div>
            <div class="nextsection"><div class="square"></div><div class="nexttext">Next Art</div></div>
        </div>
    </xsl:template>



</xsl:stylesheet>

