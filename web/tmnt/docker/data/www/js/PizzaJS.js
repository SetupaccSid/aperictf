var user = {'name': 'Michelangelo', 'pizza_stock': 0, 'eated_pizza' : 0};

function wait(ms){
   var start = new Date().getTime();
   var end = start;
   while(end < start + ms) {
     end = new Date().getTime();
  }
}

window.onload = function(e){ 

	p = document.querySelectorAll("pizza");

	for(var i = 0; i < p.length; i++) {
		if (p[i].getAttribute('cook') !== null) {
			if (p[i].getAttribute('nb') !== null) {
				user.pizza_stock += parseInt(p[i].getAttribute('nb'), 10);	
			} else {
				user.pizza_stock += 1;
			}
		} else if (p[i].getAttribute('user') !== null) {
			user.name = p[i].getAttribute('user');
		} else if (p[i].getAttribute('eat') !== null && p[i].getAttribute('nb') !== null) {
			if (parseInt(p[i].getAttribute('nb'), 10) <= user.pizza_stock) {
				user.eated_pizza += parseInt(p[i].getAttribute('nb'), 10);
				user.pizza_stock -= parseInt(p[i].getAttribute('nb'), 10);
			} else {
				console.log("Not enough pizza :'(");
			}
			if (user.eated_pizza === 1337) {
				console.log(user.name + " can't eat as much as he wants, he needs to take a break...");
				setTimeout('user.eated_pizza = 0; console.log("' + user.name + ' digested everything!")', 3000);
			}
		}
	}
}
