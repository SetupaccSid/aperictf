<?php
session_start();
error_reporting(0);
ini_set('display_errors', 0);
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

/* Check png file*/

$tmp = explode(".", $_FILES["file"]["name"]);
$fextension = strtolower(end($tmp));

if ($fextension !== "png"){
    echo("File must be a valid PNG.");
}
elseif ($_FILES["file"]["type"] !== "image/png"){
    echo("File must be a valid PNG.");
}
elseif ($_FILES["file"]["size"] > 200000){
    echo("File is too big.");
}
elseif ($_FILES["file"]["error"] > 0){
    echo("Return Code: " . $_FILES["file"]["error"].".");
// }elseif ($_FILES["file"]["name"] !== "s3cr3tk3y.png"){
//    echo("Wrong file name.");
}else{
    
    // Build str from img
    $size = getimagesize($_FILES["file"]["tmp_name"]);
    if ($size[2] !== 3){
        echo("Wrong number of layers.");
        exit();
    }
    
    $img = imagecreatefrompng($_FILES["file"]["tmp_name"]);
    $rgbstr = "";
    for ($h=0;$h<$size[1];$h++){
        for ($w=0;$w<$size[0];$w++){
            $rgb = imagecolorat($img, $w, $h);
            $r = ($rgb >> 16) & 0xFF;
            $g = ($rgb >> 8) & 0xFF;
            $b = $rgb & 0xFF;
            $rgbstr .= chr($r);
            $rgbstr .= chr($g);
            $rgbstr .= chr($b);
        }
    }
    
    if (strlen($rgbstr) == 0){
        echo("Key error.");
        exit();
    }
    
    $creds = explode(":",$rgbstr);
    if (sizeof($creds) < 3){
        echo("Key error. Expected at least 3 blocks, got ".sizeof($creds).".");
        exit();
    }
    
    $mysqli = mysqli_connect("whattheauth-db", "user", "2cWL2auRtd33pRBuFAjDqTq3MGsUEHgZ", "accounts");
    
    $query = 'SELECT * FROM accounts WHERE user = "'.$creds[0].'" and passwd = "'.$creds[1].'";';
    
    $returned_set = $mysqli->query($query);
    while($result = $returned_set->fetch_row()) {
        $_SESSION['logged'] = True;
        $_SESSION['user'] = htmlspecialchars(@$result[0], ENT_QUOTES, 'UTF-8');
        $_SESSION['passwd'] = htmlspecialchars(@$result[1], ENT_QUOTES, 'UTF-8');
        $_SESSION['description'] = htmlspecialchars(@$result[2], ENT_QUOTES, 'UTF-8');
        echo("redirect");
        exit();
    }
    echo("Invalid credentials");
}
?>
