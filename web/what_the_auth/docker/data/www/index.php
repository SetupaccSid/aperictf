<?php
session_start();
?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8" />
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet"> 
    <link rel="stylesheet" href="files/style.css"/>
    <link rel="shortcut icon" href="files/logo.png">
    <title>What The Auth</title>
</head>
<body>
    <form id="form">
        <div id="circlekey"></div><br>
        <div id="filebutton">Key file...</div><br>
        <div id="submitbutton">Connect</div><br>
        <input id="file" name="file" type="file"/>
        <div id="error"></div>
    </form>
<script src="files/jquery-3.4.0.min.js"></script>
<script src="files/js.js"></script>
</body>
</html>
