<?php

session_start();
require_once("waf_6XlaFiKrI6Dqntxy.php");

$lang = @explode(",",@$_SERVER['HTTP_ACCEPT_LANGUAGE'])[0];

if (@$_SESSION['admin'] === True){
    header("Location: /f40WMzfTVfY5r3Tj/");
    exit();
}else{
    include($lang.".php"); // Aperi'Kube: Hope you used php://filter/convert.iconv.utf-8.utf-16/resource=
}
?>
<!DOCTYPE html>
<html lang="<?= @noxss($lang) ?>">
    <head prefix="og: http://ogp.me/ns#">
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <title>Worldmeet 🌍</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Beth+Ellen|Open+Sans&display=swap" rel="stylesheet">
        <link href="style.css" rel="stylesheet">
    </head>
    <body><div id="scene"><div id="bg" data-depth="0.2"></div></div>
    <header><?= @$headline ?></header>
    <img id="flag" src="<?= @$flag; ?>"/>
    <div id="online"><h1><?= @$online ?></h1>
        <div class="people"><span class="color_F">Lily - Boston - USA</span></div>
        <div class="people"><span class="color_M">Piotr - Warsaw - Poland</span></div>
        <div class="people"><span class="color_F">Olga - Lviv - Ukraine</span></div>
        <div class="people"><span class="color_M">Robert - Bucharest - Romania</span></div>
        <div class="people"><span class="color_M">Ahmet - Istanbul - Turkey</span></div>
        <div class="people"><span class="color_M">Mohamed - Cairo - Egypt</span></div>
        <div class="people"><span class="color_F">Bella - Tucson - USA</span></div>
        <div class="people"><span class="color_M">Aldo - Turin - Italy</span></div>
        ...
    </div><!--
    --><div id="logininsc">
        <div id="login">
            <h1>Login</h1>
            <?= @$login ?>: <input type="text" placeholder="login"/>
            <?= @$password ?>: <input type="password" placeholder="password"/>
            <input type="submit" value="<?= @$connect; ?>"/>
        </div>
    </div>
    <script>
        var scene = document.getElementById('scene');
        var parallaxInstance = new Parallax(scene);</script>
    </body>
</html>
