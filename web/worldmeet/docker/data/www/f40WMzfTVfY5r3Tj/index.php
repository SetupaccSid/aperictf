<?php
session_start();

require_once("../waf_6XlaFiKrI6Dqntxy.php");
$lang = @explode(",",@$_SERVER['HTTP_ACCEPT_LANGUAGE'])[0];
include("../".$lang.".php");

$link = mysqli_connect("worldmeet-db", "user", "XoMOFVtYFKRJeB75BwxQ4HGMCpNFolWIDMnhrnaa", "accounts");
$req = "SELECT * FROM accounts WHERE user=?";

/*
CREATE TABLE `accounts`  (
  `user`
  `passwd`
  `description`
);
*/


function secure_hash($p){
    return sha1($p,"my_s3cure_salt");  // sha1(new.salt) in hexa
}

if (isset($_GET['logout'])){
        $_SESSION['admin'] = False;
        header("Location: ../"); // Admin page
        exit();
}

if (isset($_POST['user']) && isset($_POST['pass'])){
    $user = $_POST['user'];
    $pass = $_POST['pass'];
    $req .= " AND passwd LIKE '".secure_hash($pass)."';";
    $stmt = $link->prepare($req);
    $stmt->bind_param("s", $user);
    $stmt->execute();
    $result = $stmt->get_result()->fetch_assoc();
    $stmt->close();

    if($result){
        $_SESSION['admin'] = True;
    }else{
        $_SESSION['admin'] = False;
        header("Location: ../"); // Admin page
        exit();
    }
}

?>
<!DOCTYPE html>
<html lang="<?= @noxss($lang) ?>">
    <head prefix="og: http://ogp.me/ns#">
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <title>Worldmeet 🌍</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link href="https://fonts.googleapis.com/css?family=Beth+Ellen|Open+Sans&display=swap" rel="stylesheet"> 
        <link href="../style.css" rel="stylesheet">
    </head>
    <body><div id="bg"></div>
    <header>Admin</header>
    <img id="flag" src="../<?= @$flag; ?>"/>
    <?php if (@$_SESSION['admin'] === True){ ?>
        <div id="online" style="margin-right: 0;">
        Welcome Admin !<br>
        Page is still under development.<!--
        - <a href="debug_Mm9vFfnE4H7b3WP2.php">Go to debug page</a>
        -->
        </div>
    <?php }else{
    ?>
    <div id="login"><form action="" method="POST">
        Username:<br/>
        <input type="text" name="user" id="inp_user"/><br/>
        Password:<br/>
        <input type="password" name="pass" id="inp_pass"/><br/>
        <input type="submit" value="<?= @$connect; ?>"/>
    </form></div>
    <?php } ?>
    </body>
</html>
