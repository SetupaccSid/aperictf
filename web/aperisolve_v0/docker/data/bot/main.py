from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import time
from threading import Thread

admin_token = '896ef65f009be190c6346d3bc7eaa84764ce2217efaee49cf8f1c0e31f969cff'
host = "aperisolve-nginx"
url = "http://"+host+"/" # /rapport

def trig(drv):
    drv.get(url+admin_token+".php")

while(True):
    try:
        driver = webdriver.Remote(command_executor='http://aperisolve-selenium:4444/wd/hub',
                                  desired_capabilities=DesiredCapabilities.FIREFOX)
        t = Thread(target=trig, args=(driver,))
        t.start()
        time.sleep(10)  # slow down
        driver.quit()
    except Exception as e:
        print(e)

