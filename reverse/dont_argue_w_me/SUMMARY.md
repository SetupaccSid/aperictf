# Don't argue with me

L'un de vos collègues un peu joueur a conçu un système de protection de binaire basé sur un technique qu'il estime redoutable.

Prouvez-lui qu'il a tort.

<u>**Fichier&nbsp;:**</u> [chall](files/chall) - md5sum: 89c49b3757a36d8e304ed675a607ee20
