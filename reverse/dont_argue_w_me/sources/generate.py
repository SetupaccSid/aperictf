import random
import sys
from z3 import *

flag = "APRK{Did_u_thought_that_it_would_be_easy_without_scripting?}"

def generate_xor(i,value,arg=None):
	if(arg==None):
		out = random.randint(0,255)
		cipher =  value ^ out
		return ' (out[' + str(i) + '] ^ ' + str(cipher) + ') == ' + str(out)
	else:
		cipher =  value ^ arg[1]
		return ' (out[' + str(i) + '] ^ ' + str(cipher) + ') == out[' + str(arg[0]) + ']'


def generate_add(i,value,arg=None):
	if(arg==None):
		out = random.randint(0,255)
		cipher =  value + out
		return ' (out[' + str(i) + '] + ' + str(out) + ') == ' + str(cipher) 
	else:
		cipher =  value + arg[1]
		return ' (out[' + str(i) + '] + out[' + str(arg[0]) + '])  == ' + str(cipher)

def generate_rshift(i,value,arg=None):
	if(arg==None):
		out = random.randint(0,255)
		cipher =  value >> out % 8
		return ' (out[' + str(i) + '] >> ' + str(out) + ' % 8) == ' + str(cipher) 
	else:
		cipher =  value >> arg[1] % 8
		return ' (out[' + str(i) + '] >> out[' + str(arg[0]) + '] % 8) == ' + str(cipher)

def generate_lshift(i,value,arg=None):
	if(arg==None):
		out = random.randint(0,255)
		cipher =  value << out % 8
		return ' (out[' + str(i) + '] << ' + str(out) + ' % 8) == ' + str(cipher) 
	else:
		cipher =  value << arg[1] % 8
		return ' (out[' + str(i) + '] << out[' + str(arg[0]) + '] % 8) == ' + str(cipher)

def generate_sub(i,value,arg=None):
	if(arg==None):
		out = random.randint(0,255)
		cipher =  value - out
		return ' (out[' + str(i) + '] - ' + str(out) + ') == ' + str(cipher) 
	else:
		cipher =  value - arg[1]
		return ' (out[' + str(i) + '] - out[' + str(arg[0]) + '])  == ' + str(cipher)

def generate_and(i,value,arg=None):
	#TODO: Make it work
	if(arg==None):
		out = random.randint(0,255)
		cipher =  value & out
		return ' (out[' + str(i) + '] & ' + str(out) + ') == '  + str(cipher) 
	else:
		cipher =  value & arg[1]
		return ' (out[' + str(i) + '] & out[' + str(arg[0]) + ']'  + ') == ' + str(cipher)

cond = []
# generate_rshift, generate_lshift,
functions_generate = [generate_xor,generate_add,  generate_sub, generate_rshift, generate_lshift, ] 
start = 0
end = len(flag)-1

s = Solver()
out = [BitVec("%i" % i, 8) for i in range(len(flag))]
print_out = ""
desired_nb_flag = 1
min_cond = len(flag) * 2 + 55


def get_models(s):
	models = []
	min_mod_count = 2
	while s.check() == sat and len(models) < min_mod_count:
	  m = s.model()
	  models.append(m)
	  s.add(Or([sym() != m[sym] for sym in m]))
	return models

print("")
nb_flag = -1
print("======== Generating conditions ============")
print("Potential flags :")
while len(cond) < min_cond  or nb_flag > desired_nb_flag :
	index = random.randint(start,end)
	arg = random.randint(0,1)
	callback = functions_generate[random.randint(0,len(functions_generate)-1)]
	
	'''
	Adding previous cond
	'''
	if(len(cond) >= min_cond):
		for i in cond:
			eval(i)
		
		for i in out : 
			s.add(i != ord('\x00'))
			s.add(i >= ord(' '))
			s.add(i <= ord('~'))

	'''
	Generating new cond
	'''
	if(arg):
		second_index = random.randint(start,end)
		current_cond = callback(index, ord(flag[index]),[second_index,ord(flag[second_index])])
		
		cmd = "s.add(" + current_cond + ")"
		cond.append(cmd)
		eval(cmd)
		print_out += current_cond + ' &&\n'
	else:
		current_cond = callback(index, ord(flag[index]))
		cmd = "s.add(" + current_cond + ")"
		cond.append(cmd)
		eval(cmd)
		print_out +=  current_cond + ' &&\n'

	'''
	Getting nb_flags
	'''
	print("Current nb of conds : " + str(len(cond)))

	if(len(cond) >= min_cond):
		nb_flag = len(get_models(s))
	
	s.reset()

for i in cond:
	eval(i)
	
for i in out : 
	s.add(i != ord('\x00'))
	s.add(i >= ord(' '))
	s.add(i <= ord('~'))

for m in get_models(s):
	pwd = list("a" * len(flag)) # dummy list
	for name in m:
			pwd[int(name.__str__())] = chr(m[name].as_long())
	print("".join(pwd))


print("========= Conditions ============")

print(print_out[:-3])

print("========= Writing conditions ===============")
print("Current nb of conds : " + str(len(cond)))

f = open("chall.c","w")
f.write("""
#include <stdio.h>
void main(int argc, char** argv){
	char *out = argv[0];
	if(
		"""
		+
		print_out[:-3]
		+
		"""
	){
		printf("Well done ! You validated this challenge ! ");
	} else{
		printf("Bwahhh ! Wrong flag !");
	}
}
""")

f.close()