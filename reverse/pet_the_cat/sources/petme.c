#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//gcc petme.c -o petme
//APRK{CRC32_1s_r3v3rs1bl3_;)}

unsigned int dosomemagic(char *message) {
   int i, j;
   unsigned int ccc, aaa, bbb;

   i = 0;
   aaa = 0xFFFFFFFF;
   while (message[i] != 0) {
      ccc = message[i];            // Get next ccc.
      aaa = aaa ^ ccc;
      for (j = 7; j >= 0; j--) {    // Do eight times.
         bbb = -(aaa & 1);
         aaa = (aaa >> 1) ^ (0xEDB88320 & bbb);
      }
      i = i + 1;
   }
   return ~aaa;
}

void pet(char *in, char *out) {
    int i, n;
    n = strlen(in);
    char ccc[5];
    unsigned int crypted;
    for (i=0;i<160 && i<n;i+=4) {
        strncpy(ccc, (in+i), 4);
        ccc[4] = '\0';
        crypted = dosomemagic(ccc);
        strncpy((out+i), (char *)&crypted, 4);
    }
}

int main(int argc, char* argv[]) {

    char flag[] = "\x3a\x69\xd3\x07\xe4\xe4\xfe\x88\x0c\xf8\xc8\x88\x81\xc3\xf7\x9d\x67\x40\x38\x9d\x2b\x1a\x18\x54\xf3\x58\xe6\xf0";

    if (argc != 2) {
        printf("Usage: %s <password>\n", argv[0]);
        return 0;
    }
    char buffer[200];
    memset(buffer, '\0', 200);
    pet(argv[1], buffer);

    if (strlen(buffer) == strlen(flag) && memcmp(buffer, flag, strlen(flag)) == 0) {
        printf("You pet the cat!\n");
    }
    else {
        printf("The cat doesn't want to be pet!\n");
    }

    return 0;
}
