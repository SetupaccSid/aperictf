#include <stdio.h>
#include <time.h>
#include <stdlib.h>

// Flag = mov_your_boooOOOooody

void fail(void) {
  printf("You failed :(\n");
  exit(1);
}

int main(int argc, char ** argv) {
  int len = 0;

  if (argc != 2) {
    printf("Usage: %s password\n", argv[0]);
    return 1;
  }

  for(len=0; argv[1][len]!='\0'; len++);

  if (len <= 20) {
    fail();
  }

  if (len > 21) {
    fail();
  }

  printf("That's a good start! =]\n");

  if (argv[1][0] != 'm') {
    fail();
  }
  time(NULL);

  if (argv[1][1] != 'o') {
    fail();
  }
  time(NULL);

  if (argv[1][2] != 'v') {
    fail();
  }
  time(NULL);

  if (argv[1][3] != '_') {
    fail();
  }
  time(NULL);

  if (argv[1][4] != 'y') {
    fail();
  }
  time(NULL);

  if (argv[1][5] != 'o') {
    fail();
  }
  time(NULL);

  if (argv[1][6] != 'u') {
    fail();
  }
  time(NULL);

  if (argv[1][7] != 'r') {
    fail();
  }
  time(NULL);

  if (argv[1][8] != '_') {
    fail();
  }
  time(NULL);

  if (argv[1][9] != 'b') {
    fail();
  }
  time(NULL);

  if (argv[1][10] != 'o') {
    fail();
  }
  time(NULL);

  if (argv[1][11] != 'o') {
    fail();
  }
  time(NULL);

  if (argv[1][12] != 'o') {
    fail();
  }
  time(NULL);

  if (argv[1][13] != 'O') {
    fail();
  }
  time(NULL);

  if (argv[1][14] != 'O') {
    fail();
  }
  time(NULL);

  if (argv[1][15] != 'O') {
    fail();
  }
  time(NULL);

  if (argv[1][16] != 'o') {
    fail();
  }
  time(NULL);

  if (argv[1][17] != 'o') {
    fail();
  }
  time(NULL);

  if (argv[1][18] != 'o') {
    fail();
  }
  time(NULL);

  if (argv[1][19] != 'd') {
    fail();
  }
  time(NULL);

  if (argv[1][20] != 'y') {
    fail();
  }
  time(NULL);

  printf("Gg mate, flag is APRK{%s}. \n", argv[1]);
  return 0;
}
