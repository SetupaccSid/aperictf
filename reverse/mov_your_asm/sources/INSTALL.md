# MovYourAsm

## Installation

```bash
# Build l'image et le crackme
docker build -t mov_your_asm .
# Acceder au contenu du docker pour copier le crackme
docker run -it mov_your_asm
# Récupérer l'id du process docker
docker ps
# Recupérer le binaire compilé
docker cp <DOCKER_PS_ID>:/home/mov_your_asm ./aperictf/reverse/mov_your_asm
```

## Test

```bash
# Must fail
./mov_your_asm
# Must fail
./mov_your_asm mov_your_booo___ooody
# Must fail
./mov_your_asm mov_your_boooOOOooodyy
# Must win
./mov_your_asm mov_your_boooOOOooody
```


## Exploit output

```python
./exploit.py
# Found len: 21
# password m
# password mo
# password mov
# password mov_
# password mov_y
# password mov_yo
# password mov_you
# password mov_your
# password mov_your_
# password mov_your_b
# password mov_your_bo
# password mov_your_boo
# password mov_your_booo
# password mov_your_boooO
# password mov_your_boooOO
# password mov_your_boooOOO
# password mov_your_boooOOOo
# password mov_your_boooOOOoo
# password mov_your_boooOOOooo
# password mov_your_boooOOOoood
# password mov_your_boooOOOooody
# Found Password: mov_your_boooOOOooody
```
