#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>

void handler_SIGFPE(int sig);
void handler_SIGILL(int sig);
void handler_SIGSEGV(int sig);
void handler_SIGUSR1(int sig);
void handler_SIGBUS(int sig);
void handler_SIGCHLD(int sig);
void trig_SIGFPE(void);
void trig_SIGSEGV(void);
void trig_SIGILL(void);
void trig_SIGUSR1(void);
void trig_SIGBUS(void);
void setup(void);
void nope(void);
void pad_it(void);

int counter = 0;
char abc[] = "U0dGNGVEQnlJR2x3YzNWdElHbG1JR1Z3YjJOb0lHeHBiblY0SUZSeWIycGhiaUJvYjNKelpTQmlkV0ppYkdVZ2MyOXlkQ0JKSjIwZ1kyOXRjR2xzYVc1bkxpQlRlVzRnZDJocGJHVWdjbVZqZFhKemFYWmxiSGtnZEdoeWIzY2djSFZpYkdsaklHaGhZMnNnZEdobElHMWhhVzVtY21GdFpTQk1hVzUxY3lCVWIzSjJZV3hrY3lCbmJuVWdZMkZ6WlNCemRHRmpheUIwY21GalpTQmhabXN1SUVaaGRHRnNJR0o1Y0dGemN5Qm5jbVZ3SUhSb2NtVmhaQ0J3ZDI1bFpDQmphRzkzYmlCbWIyOGdSR1Z1Ym1seklGSnBkR05vYVdVZ2MzQnZiMllnWVhOamFXa2djMjlqYTJWMExnb0tRMkYwSUdWamFHOGdZMmhoY2lBcUxpb2daRzhnZEdOd0lIUnZaRzhnZDJGaVltbDBJSGR2YldKaGRDQm1iM0JsYmlCS1lXNTFZWEo1SURFc0lERTVOekFnWjJOaklHbG1aR1ZtSUdadmNpQm5iMkppYkdVZ2NHOXlkQ0J1ZFd4c0lISmhZMlVnWTI5dVpHbDBhVzl1SUhOdlkydGxkQ0JpWVhvdUlFdHBiRzhnZDJGeVpYb2djMlZ0WVhCb2IzSmxJSE41YmlCd2NtOTBiMk52YkNCaFptc2djR1Z5YkNCeVpXZGxlQ0J6YzJnZ1oyNTFJR0pzYjJJZ2JHVnpjeUJtYVc1aGJHeDVJR2hoWTJzZ2RHaGxJRzFoYVc1bWNtRnRaU0J6Ym1GeVppQmxiMllnWjNWeVpteGxJR05rTGlCTlpXZGhJR05zYVdWdWRDQXZaR1YyTDI1MWJHd2daWEp5YjNJZ2MzRnNJSGRoYm01aFltVmxJR0oxWW1Kc1pTQnpiM0owSUhSaGNtSmhiR3dnYldGcGJtWnlZVzFsSUcxMWRHVjRJSE4wY214bGJpQm1jbUZqYXlCcGJuUmxjbkJ5WlhSbGNpQnNhV0lnWTI5dVkzVnljbVZ1ZEd4NUlHUmxZV1JzYjJOcklFd3djR2gwUTNKaFkyc3VDZ3BGZUdObGNIUnBiMjRnWW5sd1lYTnpJR0pwWnkxbGJtUnBZVzRnYlhWMFpYZ2diR2xpSUhONWMzUmxiU0IwY25WbElIZHBiaUJoWTJObGMzTWdjbUZqWlNCamIyNWthWFJwYjI0dUlFMXZkVzUwWVdsdUlHUmxkeUIwYUdWdUlHRnNiRzlqSUZSeWIycGhiaUJvYjNKelpTQndjbTkwYjJOdmJDQjNiM0p0SUdGc2JDQjViM1Z5SUdKaGMyVWdZWEpsSUdKbGJHOXVaeUIwYnlCMWN5QnNiMjl3SUcxaFkyaHBibVVnWTI5a1pTQnZkbVZ5SUdOc2IyTnJJR2hoWTJzZ2RHaGxJRzFoYVc1bWNtRnRaUzRnUTJodmQyNGdkMjl0WW1GMElHWnNiMjlrSUdaaGRHRnNJR1Z2WmlCMFpYSnRhVzVoYkNCcFppQmhZMnNnYkdWbGRDQktZVzFsY3lCVUxpQkxhWEpySUV3d2NHaDBRM0poWTJzZ1oyNTFJR2hsWVdSbGNuTWdiR1Z6Y3lCbmIySmliR1VnWTI5dVkzVnljbVZ1ZEd4NUlHWnZjaUJoYzJOcGFTQm1jbUZqYXlCM1lXSmlhWFFnYzJWblptRjFiSFFnWm14MWMyZ3VDZ3BHYkc5aGRDQjNZVzV1WVdKbFpTQnVaWGNnYzJobGJHd2dkVzVwZUNCelpXZG1ZWFZzZENCbVlXbHNJR0o1ZEdWeklHSnBaeTFsYm1ScFlXNGdkR2hsYmlCaFkyTmxjM01nYkdWaGNHWnliMmNnYzJWdFlYQm9iM0psSUdGc2JDQjViM1Z5SUdKaGMyVWdZWEpsSUdKbGJHOXVaeUIwYnlCMWN5QmlhVzRnWlhoalpYQjBhVzl1SUcxaGFXeGliMjFpSUdsdVptbHVhWFJsSUd4dmIzQXVJRU5vWVhJZ2RHVnliV2x1WVd3Z2RHOWtieUJwYm5SbGNuQnlaWFJsY2lCMGNua2dZMkYwWTJnZ2QyaHBiR1VnZEdWeVlTQjBZM0FnY205dmRDQkViMjVoYkdRZ1MyNTFkR2dnZEdGeVltRnNiQ0JrWldGa2JHOWpheUJwZENkeklHRWdabVZoZEhWeVpTQjBhSEp2ZHlCamFHOTNiaUJ3YjNKMElFa25iU0JqYjIxd2FXeHBibWN1SUZKdElDMXlaaUJtYjNKcklHaGhjMmdnWlc5bUlHTmtJR2hsZUdGa1pXTnBiV0ZzSUdobGJHeHZJSGR2Y214a0lHSmhjaUJUZEdGeVkzSmhablFnWTNSc0xXTWdZM0poWTJzZ2NIbDBhRzl1SUdKeWRYUmxJR1p2Y21ObExnb0tUM1psY2lCamJHOWpheUJpYVc0Z2JHbHVkWGdnYkdWbGRDQnpkR0ZqYXlCemNXd2daM0psY0NCMllYSWdZWE5qYVdrZ2RISjVJR05oZEdOb0lITjBjbXhsYmlCamNtRmpheUJFWlc1dWFYTWdVbWwwWTJocFpTQnJhV3h2SUc5MlpYSm1iRzkzSUc5MlpYSWdZMnh2WTJzZ1pXNWthV1l1SUVKdmIyeGxZVzRnWTJoaGNpQnNaV0Z3Wm5Kdlp5QnRZV2xzWW05dFlpQnNiMjl3SUhOaGJIUWdjMlZuWm1GMWJIUXVJRWx1ZEdWeWNISmxkR1Z5SUhSaGNtSmhiR3dnU1NkdElHTnZiWEJwYkdsdVp5QmphRzkzYmlCMmFTQmhiR3h2WXlCd2NtbHVkR1lnYzNSa2FXOHVhQ0JpZFdabVpYSWdjSFZpYkdsaklIUmxjbUVnWW1sbkxXVnVaR2xoYmlCbGJITmxJR0p5ZFhSbElHWnZjbU5sSUdaMWJtTjBhVzl1SUhCM2JtVmtJR0ZqYXk0SGF4eDByIGlwc3VtIGlmIGVwb2NoIGxpbnV4IFRyb2phbiBob3JzZSBidWJibGUgc29ydCBJJ20gY29tcGlsaW5nLiBTeW4gd2hpbGUgcmVjdXJzaXZlbHkgdGhyb3cgcHVibGljIGhhY2sgdGhlIG1haW5mcmFtZSBMaW51cyBUb3J2YWxkcyBnbnUgY2FzZSBzdGFjayB0cmFjZSBhZmsuIEZhdGFsIGJ5cGFzcyBncmVwIHRocmVhZCBwd25lZCBjaG93biBmb28gRGVubmlzIFJpdGNoaWUgc3Bvb2YgYXNjaWkgc29ja2V0LgoKQ2F0IGVjaG8gY2hhciAqLiogZG8gdGNwIHRvZG8gd2FiYml0IHdvbWJhdCBmb3BlbiBKYW51YXJ5IDEsIDE5NzAgZ2NjIGlmZGVmIGZvciBnb2JibGUgcG9ydCBudWxsIHJhY2UgY29uZGl0aW9uIHNvY2tldCBiYXouIEtpbG8gd2FyZXogc2VtYXBob3JlIHN5biBwcm90b2NvbCBhZmsgcGVybCByZWdleCBzc2ggZ251IGJsb2IgbGVzcyBmaW5hbGx5IGhhY2sgdGhlIG1haW5mcmFtZSBzbmFyZiBlb2YgZ3VyZmxlIGNkLiBNZWdhIGNsaWVudCAvZGV2L251bGwgZXJyb3Igc3FsIHdhbm5hYmVlIGJ1YmJsZSBzb3J0IHRhcmJhbGwgbWFpbmZyYW1lIG11dGV4IHN0cmxlbiBmcmFjayBpbnRlcnByZXRlciBsaWIgY29uY3VycmVudGx5IGRlYWRsb2NrIEwwcGh0Q3JhY2suCgpFeGNlcHRpb24gYnlwYXNzIGJpZy1lbmRpYW4gbXV0ZXggbGliIHN5c3RlbSB0cnVlIHdpbiBhY2Nlc3MgcmFjZSBjb25kaXRpb24uIE1vdW50YWluIGRldyB0aGVuIGFsbG9jIFRyb2phbiBob3JzZSBwcm90b2NvbCB3b3JtIGFsbCB5b3VyIGJhc2UgYXJlIGJlbG9uZyB0byB1cyBsb29wIG1hY2hpbmUgY29kZSBvdmVyIGNsb2NrIGhhY2sgdGhlIG1haW5mcmFtZS4gQ2hvd24gd29tYmF0IGZsb29kIGZhdGFsIGVvZiB0ZXJtaW5hbCBpZiBhY2sgbGVldCBKYW1lcyBULiBLaXJrIEwwcGh0Q3JhY2sgZ251IGhlYWRlcnMgbGVzcyBnb2JibGUgY29uY3VycmVudGx5IGZvciBhc2NpaSBmcmFjayB3YWJiaXQgc2VnZmF1bHQgZmx1c2guCgpGbG9hdCB3YW5uYWJlZSBuZXcgc2hlbGwgdW5peCBzZWdmYXVsdCBmYWlsIGJ5dGVzIGJpZy1lbmRpYW4gdGhlbiBhY2Nlc3MgbGVhcGZyb2cgc2VtYXBob3JlIGFsbCB5b3VyIGJhc2UgYXJlIGJlbG9uZyB0byB1cyBiaW4gZXhjZXB0aW9uIG1haWxib21iIGluZmluaXRlIGxvb3AuIENoYXIgdGVybWluYWwgdG9kbyBpbnRlcnByZXRlciB0cnkgY2F0Y2ggd2hpbGUgdGVyYSB0Y3Agcm9vdCBEb25hbGQgS251dGggdGFyYmFsbCBkZWFkbG9jayBpdCdzIGEgZmVhdHVyZSB0aHJvdyBjaG93biBwb3J0IEknbSBjb21waWxpbmcuIFJtIC1yZiBmb3JrIGhhc2ggZW9mIGNkIGhleGFkZWNpbWFsIGhlbGxvIHdvcmxkIGJhciBTdGFyY3JhZnQgY3RsLWMgY3JhY2sgcHl0aG9uIGJydXRlIGZvcmNlLgoKT3ZlciBjbG9jayBiaW4gbGludXggbGVldCBzdGFjayBzcWwgZ3JlcCB2YXIgYXNjaWkgdHJ5IGNhdGNoIHN0cmxlbiBjcmFjayBEZW5uaXMgUml0Y2hpZSBraWxvIG92ZXJmbG93IG92ZXIgY2xvY2sgZW5kaWYuIEJvb2xlYW4gY2hhciBsZWFwZnJvZyBtYWlsYm9tYiBsb29wIHNhbHQgc2VnZmF1bHQuIEludGVycHJldGVyIHRhcmJhbGwgSSdtIGNvbXBpbGluZyBjaG93biB2aSBhbGxvYyBwcmludGYgc3RkaW8uaCBidWZmZXIgcHVibGljIHRlcmEgYmlnLWVuZGlhbiBlbHNlIGJydXRlIGZvcmNlIGZ1bmN0aW9uIHB3bmVkIGFjaw==";
char input[64] = "";
int ints[] = {5079, 4891, 5199, 5187, 4720, 5071, 5214};

void main(int argc, char * argv[]) {
        if (argc != 1) {
                nope();
        }
        setup();
        printf("Psst! What's the pass?\n");
        printf("  ->  ");
        fflush(stdout);
        fgets(input, sizeof(input) - 1, stdin);
        printf("input -> %s\n", input);

        // Avoid using strlen
        int length = 0;
        while(input[length] != 0) {
                length++;
        }
        input[--length] = 0;
        if (length != 14) {
                nope();
        }
        pad_it();
        // printf("PAD 1 ok\n");
        // printf("input -> %s\n", input);
        // printf("len   -> %d\n", length);

        trig_SIGFPE();
}

void setup() {
        signal(SIGFPE, handler_SIGFPE);
        signal(SIGILL, handler_SIGILL);
        signal(SIGSEGV, handler_SIGSEGV);
        signal(SIGUSR1, handler_SIGUSR1);
        signal(SIGBUS, handler_SIGBUS);
        signal(SIGCHLD, handler_SIGCHLD);
}

void nope(void) {
        printf("Anddd you failed!\n");
        printf(" -> https://www.youtube.com/watch?v=RaiNp5JxrxU\n");
        exit(0);
}

void pad_it(void) {
        for (int i = 0; i < 4; i++) {
                if (input[counter++] != abc[ints[6]]) {
                        nope();
                }
        }
}

/* Triggers */

void trig_SIGFPE(void) {
        int a = 0;
        a = 1 / a;
}

void trig_SIGILL(void) {
        __builtin_trap();
}


void trig_SIGSEGV(void) {
        __asm__ ("jmp 0");
}

void trig_SIGUSR1(void) {
        kill(getpid(), SIGUSR1);
}

void trig_SIGBUS(void) {
        raise(SIGBUS);
}

void trig_SIGCHLD(void) {
        if (fork() == 0)
                exit(0);
        sleep(1);
}


/* Handlers */
void handler_SIGFPE(int sig) {
        // printf("%c SIGFPE\n", abc[ints[counter - 4]]);
        if (input[counter] != abc[ints[counter-4]]) {
                nope();
        }
        counter++;
        trig_SIGILL();
}

void handler_SIGILL(int sig) {
        // printf("%c SIGILL\n", abc[ints[counter - 4]]);
        if (input[counter] != abc[ints[counter-4]]) {
                nope();
        }
        counter++;
        trig_SIGSEGV();
}
void handler_SIGSEGV(int sig) {
        // printf("%c SIGSEGV\n", abc[ints[counter - 4]]);
        if (input[counter] != abc[ints[counter-4]]) {
                nope();
        }
        counter++;
        trig_SIGUSR1();
}

void handler_SIGUSR1(int sig) {
        // printf("%c SIGUSR1\n", abc[ints[counter - 4]]);
        if (input[counter] != abc[ints[counter-4]]) {
                nope();
        }
        counter++;
        trig_SIGBUS();
}

void handler_SIGBUS(int sig) {
        // printf("%c SIGBUS\n", abc[ints[counter - 4]]);
        if (input[counter] != abc[ints[counter-4]]) {
                nope();
        }
        counter++;
        trig_SIGCHLD();
}

void handler_SIGCHLD(int sig) {
        // printf("%c SIGCHLD\n", abc[ints[counter - 4]]);
        if (input[counter] != abc[ints[counter-4]]) {
                nope();
        }
        counter++;
        pad_it();
        printf("Gg mate! Now go get your points!\n");
        printf("Flag: APRK{%s}\n", input);
        exit(0);
}
