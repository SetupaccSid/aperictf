#pragma once
#include <stdlib.h>

typedef struct _matrix_t
{
	int nrows;
	int ncols;
	double data[];
}matrix_t;


matrix_t* createEmptyMatrix(int nrows, int ncols);
matrix_t* createMatrix(double* data, int nrows, int ncols);
void displayMatrix(matrix_t* matrix);