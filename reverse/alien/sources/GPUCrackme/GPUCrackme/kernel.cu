
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "matrix.h"

cudaError_t matrixMulWithCuda(matrix_t* A,matrix_t* B,matrix_t** C);

__global__ void _8d081f4b2b766e92b4ab59bcf4a9b620(double* a,double* b,double* c,int N)
{
	// (m,n) * (n,p) = (m,p)
	// blockDim.x => nombre de colonnes 
	// blockDim.y => nombre de ligne 

	int cellIdx = threadIdx.y * blockDim.x + threadIdx.x;
	c[cellIdx] = 0;
	for (int i = 0; i < N; i++)
	{
		c[cellIdx] += a[ threadIdx.y * N + i] * b[ i * blockDim.x + threadIdx.x ];
	}
}


bool checkFlag(double* a, double* b, int size)
{
	bool bValid = true;

	for (int i = 0; i < size; i++)
	{
		if (a[i] != b[i])
		{
			//printf("%d %ld != %ld\n",i, encrypted_password[i], flag[i]);
			bValid = false;
			break;
		}
	}

	return bValid;
}

int main()
{

	cudaError_t cudaStatus;
	// matrix_is_awesome_on_gpu! 5x5
	double key[] =
	{
		109,  97, 116, 114, 105,
		120,  95, 105, 115,  95,
		97, 119, 101, 115, 111,
		109, 101,  95, 111, 110,
		95, 103, 112, 117,  33
	};
	
	// Aperi'Kube{Did_you_see_GPU_instructions?}

	/*
	double flag[] =
	{
		53686.00000000,
		52130.00000000,
		54599.00000000,
		52196.00000000,
		45826.00000000,
		46875.00000000,
		44955.00000000,
		47006.00000000,
		44929.00000000,
		39333.00000000,
		53558.00000000,
		52770.00000000,
		52673.00000000,
		51800.00000000,
		45284.00000000,
		60433.00000000,
		59200.00000000,
		60453.00000000,
		58710.00000000,
		50942.00000000,
		48320.00000000,
		47455.00000000,
		48456.00000000,
		46916.00000000,
		41585.00000000,
		55275.00000000,
		53825.00000000,
		55570.00000000,
		53695.00000000,
		46285.00000000,
		60740.00000000,
		59440.00000000,
		60896.00000000,
		59022.00000000,
		51277.00000000,
		54697.00000000,
		53905.00000000,
		54722.00000000,
		52801.00000000,
		49262.00000000,
		13625.00000000,
		15000.00000000,
		12125.00000000,
		13625.00000000,
		11875.00000000,
	};
	*/

	// APRK{Did_you_see_GPU_instructions?}
	double flag[] = {
		45822.000000,
		44320.000000,
		46385.000000,
		44810.000000,
		36433.000000,
		52732.000000,
		51055.000000,
		53547.000000,
		51372.000000,
		43583.000000,
		58183.000000,
		57230.000000,
		58721.000000,
		56816.000000,
		50024.000000,
		46505.000000,
		45875.000000,
		46908.000000,
		45579.000000,
		39497.000000,
		58590.000000,
		57170.000000,
		58921.000000,
		56935.000000,
		49443.000000,
		59508.000000,
		58505.000000,
		59975.000000,
		58074.000000,
		51006.000000,
		56416.000000,
		54965.000000,
		56592.000000,
		54877.000000,
		46251.000000,
	};

	char password[256];
	double encrypted_password[256];

	memset(password, 0, 256);
	
	matrix_t* m1 = createMatrix(key,5,5);
	matrix_t* m2 = createEmptyMatrix(5,1);
	
	matrix_t* m3 = NULL;

	printf("Enter password : ");
	scanf("%60s",password);
	
	size_t pass_size = strlen(password);

	for (int round = 0; round < pass_size; round+= 5)
	{
		for(int i = 0; i < 5; i++)
			m2->data[i] = (double)password[round + i];
		
		//printf("===== message ======\n");
		//displayMatrix(m2);

		// Add vectors in parallel.
		cudaStatus = matrixMulWithCuda(m1, m2, &m3);

		//printf("===== m3 ======\n");
		//displayMatrix(m3);
		
		for (int i = 0; i < 5; i++)
		{
			encrypted_password[round + i]= m3->data[i];
			//printf("%f,\n", m3->data[i]);
		}


		free(m3);

		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "encryption failed!");
			return 1;
		}

	}
	
	if (checkFlag(flag,encrypted_password,35))
	{
		printf("The flag is %s\n", password);
	}

    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        //fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }

	free(m1);
	free(m2);
	
    return 0;
}

// Helper function for using CUDA to add vectors in parallel.
cudaError_t matrixMulWithCuda(matrix_t* A, matrix_t* B, matrix_t** C)
{
    double *dev_a = 0;
    double *dev_b = 0;
    double *dev_c = 0;


	if (A->ncols != B->nrows)
	{
		//fprintf(stderr, "invalid matrix multiplication A->ncols != B->nrows");
		return cudaSuccess;
	}

	matrix_t* result = createEmptyMatrix(A->nrows, B->ncols);
    
	cudaError_t cudaStatus;

    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Do you have a CUDA-capable GPU installed?");
        goto Error;
    }

    // Allocate GPU buffers for three vectors (two input, one output)    .
    cudaStatus = cudaMalloc((void**)&dev_c, result->ncols * result->nrows * sizeof(double));
    if (cudaStatus != cudaSuccess) {
        //fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cudaStatus = cudaMalloc((void**)&dev_a, A->ncols * A->nrows * sizeof(double));
    if (cudaStatus != cudaSuccess) {
        //fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cudaStatus = cudaMalloc((void**)&dev_b, B->ncols * B->nrows * sizeof(double));
    if (cudaStatus != cudaSuccess) {
        //fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    // Copy input vectors from host memory to GPU buffers.
    cudaStatus = cudaMemcpy(dev_a,A->data, A->ncols * A->nrows * sizeof(double), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        //fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    cudaStatus = cudaMemcpy(dev_b, B->data, B->ncols * B->nrows * sizeof(double), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        //fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    // Launch a kernel on the GPU with one thread for each element.
	dim3 threadPerBlock(result->ncols,result->nrows,1);

    _8d081f4b2b766e92b4ab59bcf4a9b620<<<1,threadPerBlock>>>(dev_a, dev_b, dev_c,A->ncols);

    // Check for any errors launching the kernel
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) {
        //fprintf(stderr, "addKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
        goto Error;
    }
    
    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) {
        //fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
        goto Error;
    }

    // Copy output vector from GPU buffer to host memory.
    cudaStatus = cudaMemcpy(result->data, dev_c, result->ncols * result->nrows * sizeof(double), cudaMemcpyDeviceToHost);
    if (cudaStatus != cudaSuccess) {
        //fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

	
Error:
    cudaFree(dev_c);
    cudaFree(dev_a);
    cudaFree(dev_b);

	*C = result;

    return cudaStatus;
}
