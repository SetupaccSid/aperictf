# Emoji

Comptez le nombre d'emoji demandé par le serveur. Vous avez 3 secondes.

<u>URI&nbsp;:</u> [https://emoji.aperictf.fr/](https://emoji.aperictf.fr/)
