<?php
session_start();
$FLAG = "APRK{Aimemoj1}";

if (isset($_POST['nb1']) && is_numeric($_POST['nb1']) && ($_POST['nb1'] === strval($_SESSION['answer1'])) &&
   isset($_POST['nb2']) && is_numeric($_POST['nb2']) && ($_POST['nb2'] === strval($_SESSION['answer2']))){
    $printflag = True;
}else{
    $printflag = False;
}
if ((time() - $_SESSION['time']) >= 3){
    $printflag = False;
    $timeerror = True;
}

$emoji = "🐶 🐱 🐭 🐹 🐰 🦊 🦝 🐻 🐼 🦘 🦡 🐨 🐯 🦁 🐮 🐷 🐸 🐵 🙈 🙉 🙊 🐒 🐔 🐧 🐦 🐤 🐣 🐥 🦆 🦢 🦅 🦉 🦚 🦜 🦇 🐺 🐗 🐴 🦄 🐝 🐛 🦋 🐌 🐚 🐞 🐜 🦗 🕷 🕸 🦂 🦟 🦠 🐢 🐍 🦎 🦖 🦕 🐙 🦑 🦐 🦀 🐡 🐠 🐟 🐬 🐳 🐋 🦈 🐊 🐅 🐆 🦓 🦍 🐘 🦏 🦛 🐪 🐫 🦙 🦒 🐃 🐂 🐄 🐎 🐖 🐏 🐑 🐐 🦌 🐕 🐩 🐈 🐓 🦃 🕊 🐇 🐁 🐀 🐿 🦔";
$emoji_arr = explode(" ",$emoji);
// random title
$x = array_rand($emoji_arr,3);
$title = $emoji_arr[$x[0]]." ".$emoji_arr[$x[1]]." ".$emoji_arr[$x[2]];

$index_search = array_rand($emoji_arr,2);
$emojisearch1 = $emoji_arr[$index_search[0]];
$emojisearch2 = $emoji_arr[$index_search[1]];

$emoji_str = "";
for($i=0;$i<rand(900,1200);$i++){
    $emoji_str .= $emoji_arr[array_rand($emoji_arr)];
}
$answer = substr_count($emoji_str,$emojisearch1);
$answer2 = substr_count($emoji_str,$emojisearch2);
$_SESSION['answer1'] = $answer;
$_SESSION['answer2'] = $answer2;
$_SESSION['time'] = time();

if ($printflag){ ?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8" />
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" href="files/style.css"/>
    <link rel="shortcut icon" href="files/logo.png">
    <title><?= $title ?></title>
</head>
<body>
    <form action="/" method="post"><div id="form">
    <?= $FLAG ?>
    </div></form>
<script src="files/jquery-3.4.0.min.js"></script>
<script src="files/js.js"></script>
</body>
</html>
<?php }else{ ?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8" />
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" href="files/style.css"/>
    <link rel="shortcut icon" href="files/logo.png">
    <title><?= $title ?></title>
</head>
<body>
    <form action="/" method="post"><div id="form">
    Renvoyer le nombre de <?= $emojisearch1 ?> et de <?= $emojisearch2 ?>.<br>
    <hr/>
    <div style="font-size:14px;"><?= $emoji_str ?></div>
    <hr/>
    <label for="nb1">Nombre de <?= $emojisearch1 ?>:</label>
    <input type="number" name="nb1"/>
    <label for="nb2">Nombre de <?= $emojisearch2 ?>:</label>
    <input type="number" name="nb2"/><?php
    if ((!empty(@$_POST['nb1'])) || (!empty(@$_POST['nb2']))){
        if (isset($timeerror)){
            echo '<div class="error">Pas assez rapide !</div>';
        }else{
            echo '<div class="error">Mauvais nombre !</div>';
        }
    }
    ?>
    Temps restant: <span id="time">03:00</span>
    <input type="submit" value="Envoyer"/>
    </div></form>
    <script>
    var timeleft = 3;
    var downloadTimer = setInterval(function(){
      timeleft -= 1;
      document.getElementById("time").innerHTML = "0"+(timeleft)+":00";
      if(timeleft <= 0)
        clearInterval(downloadTimer);
    }, 1000);
    </script>
</body>
</html>
<?php } ?>
