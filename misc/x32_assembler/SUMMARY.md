# x32 Assembler

Vous avez réussi à écrire un émulateur x32 ! Pourquoi s'arrêter en si bon chemin ?
Votre mission est maintenant d'écrire un assembleur, le logiciel qui transforme les instructions x32 en bytecode exécutable par les microcontrôleurs. À vous la richesse si vous y parvenez !

Le serveur va vous envoyer du code x32 délimité par les chaines :

```
---- START ----
---- END ----
```

Vous devrez lui renvoyer la suite de bytecode correspondant au résultat de l'assemblage. Vous pouvez être sûr que le serveur ne vous enverra que du code x32 valide.

```bash
nc x32.aperictf.fr 32324
```
