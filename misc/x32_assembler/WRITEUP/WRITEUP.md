# x32 Assembler Writeup

This is the last challenge of the x32 series. We have to write a program that will assemble x32 instructions into bytecode that can be executed.

The code is delimited between markers and we are asked for the length of the compiled bytecode. If we answer incorrectly, the program exists.

```
---- START ----
OUT 0x70
XOR SP, 0x0
SUB R4, 0x7
		PUSH 0x77
	IN 0x23
# XOR
# 0x69
				IN R2


# 0x2a
---- END ----
What length is your byte code ?
87
That's not good, don't bother sending me your garbage.
```

## General Idea

Thankfully, the server only produces correct x32 code in terms of syntax. That's a huge advantage because we don't have to deal with the numerous error handling that could occur.

To solve this problem I decided to write a Python class. All an assembler need is a table to match instructions and register to there opcode and ID and keep track of label definitions.

### Parsing

The first thing to do is to remove comments, unnecessary indentation, case and extract the instruction and it's parameters for each line.

### Handling Jumps

The most difficult part is handling jumps to a label that is defined later in the code. For labels that are defined before, we already know their address in the produced bytecode.

The solution I used is simple. When I encounter a jump to a label that is not yet defined, I put it's name in the bytecode instead of the corresponding bytecode and keep track of it in a list of unseen labels. When I encounter a label definition, I search if it is present in the unseen labels. If it's the case, I can now replace every occurence of it's name in the bytecode with the right bytecode.

### Handling instructions

There is nothing too complicated about this, it's just a matter of constructing the opcode depending on the type of arguments the instruction has.

## Solution

The complete class can be found in [assembler.py](exploit/assembler.py).

This class implements all the error handling, which makes it a bit more complicated than it should be to solve this task.

We can now use it to dialogue with the service and pass all the checks[(solve.py)](exploit/solve.py).

```
python3 solve.py
Test 1/1000 : SUCCESS
Test 2/1000 : SUCCESS
Test 3/1000 : SUCCESS
...
Test 998/1000 : SUCCESS
Test 999/1000 : SUCCESS
Test 1000/1000 : SUCCESS
You passed all the tests, here you go :
APRK{1_g0t_Th3_p0w3r_t0_c0mp1l3_4nyth1ng_N0w_g1mm3_th3_M0N3Y!!}
```

ENOENT
