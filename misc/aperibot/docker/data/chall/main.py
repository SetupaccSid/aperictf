#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import logging
import random

from telegram import InlineKeyboardMarkup, InlineKeyboardButton, ParseMode
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, MessageHandler, Filters
from config import TOKEN, WORKERS, FLAG, SECRET

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)
logger = logging.getLogger(__name__)
rng = random.SystemRandom()

game_options = ['rock', 'paper', 'scissors', 'lizard', 'spock']
game_players = dict()
game_rules = [['Scissors', 'cuts', 'Paper'],
              ['Paper', 'covers', 'Rock'],
              ['Rock', 'crushes', 'Lizard'],
              ['Lizard', 'poisons', 'Spock'],
              ['Spock', 'smashes', 'Scissors'],
              ['Scissors', 'decapitates', 'Lizard'],
              ['Lizard', 'eats', 'Paper'],
              ['Paper', 'disproves', 'Spock'],
              ['Spock', 'vaporizes', 'Rock'],
              ['Rock', 'crushes', 'Scissors']]

def love_handler(bot, update):
    """Handler for the /loveyou command."""
    logger.info('%s sent love ❤!' % (update.effective_user.name))
    update.effective_message.reply_text('We love you too %s!' % (update.effective_user.name))

def start_handler(bot, update):
    """Handler for the /start command."""
    bot.send_message(chat_id=update.effective_message.chat_id, text='Hey!')
    help_handler(bot, update)

def help_handler(bot, update):
    """Handler for the /help command."""
    message = ('I\'m the Aperi\'Bot!\n'
               'You can interact with me by sending these commands:\n\n'
               '/loveyou - Send love to Aperi\'Kube\n'
               '/flag - Get flag\n'
               '/auth - Authentication')
    bot.send_message(chat_id=update.effective_message.chat_id, text=message)

def game_callback(bot, update):
    """Handler for the game menu."""
    user = update.effective_user
    user_id = user.id

    if user_id in game_players:
        user_choice = update.callback_query.data.split(':', 1)[1]
        if user_choice in game_options:
            bot_choice = game_options[rng.randint(0, len(game_options) - 1)]

            if bot_choice == user_choice:
                game_result = 2
            else:
                logger.info('%s is playing: %s - %s' % (user.name, user_choice, bot_choice))
                for game_rule in game_rules:
                    (object1, _, object2) = game_rule
                    if object1.lower() == user_choice and object2.lower() == bot_choice:
                        game_result = 1
                        matching_rule = ' '.join(game_rule)
                    elif object1.lower() == bot_choice and object2.lower() == user_choice:
                        game_result = 0
                        matching_rule = ' '.join(game_rule)

            result_message = ''
            if game_result == 0:
                result_message += '%s... You lose THE GAME :/' % (matching_rule)
            elif game_result == 1:
                result_message += '%s... You win!' % (matching_rule)
            else:
                result_message += 'Tie!'
        else:
            result_message = '%s is not an option, play again with /flag'

        game_players[user_id]['tries'] += 1

        update.callback_query.answer(result_message)

        if game_result == 1:
            logger.info('%s got flag! (%d tries)' % (user.name, game_players[user_id]['tries']))
            bot.send_message(chat_id=update.effective_message.chat_id, text='Congratulations! Here is your flag: %s' % FLAG)
            game_players.pop(user_id)
    else:
        bot.send_message(chat_id=update.effective_message.chat_id, text='You\'re not registered! Please authenticate yourself using /auth command.')

def auth_handler(bot, update, args):
    """Handler for the /auth command."""
    if len(args) != 0:
        user = update.effective_user
        user_id = user.id
        user_pass = args[0]

        if user_pass == SECRET:
            if user_id not in game_players:
                game_players[user_id] = {'name': user.name, 'tries': 0}

            logger.info('New player %s!' % (game_players[user_id]['name']))
            bot.send_message(chat_id=update.effective_message.chat_id, text='Welcome %s!' % (user.name))
            help_handler(bot, update)
        else:
            logger.info('%s tried to authenticate itself using "%s"' % (user.name, user_pass))
            bot.send_message(chat_id=update.effective_message.chat_id, text='Bad authentication token!')
    else:
        bot.send_message(chat_id=update.effective_message.chat_id, parse_mode=ParseMode.MARKDOWN,
                         text='No token provided, please authenticate yourself using `/auth TOKEN` command!')

def flag_handler(bot, update):
    """Handler for the /flag command."""
    user = update.effective_user
    user_id = user.id

    if user_id in game_players:
        button_list = []
        for option in game_options:
            button = InlineKeyboardButton(text=option.capitalize(), callback_data='game:%s' % option)
            button_list.append(button)

        reply_markup = InlineKeyboardMarkup([button_list])
        message = ('I want to play a game, let\'s play Rock, Paper, Scissors, Lizard, Spock!'
                   '\n\n<b>RULES</b>\n\n%s' % ('\n'.join([' '.join(rule) for rule in game_rules])))
        bot.send_message(chat_id=update.effective_message.chat_id, parse_mode=ParseMode.HTML, text=message, reply_markup=reply_markup)
    else:
        bot.send_message(chat_id=update.effective_message.chat_id, parse_mode=ParseMode.MARKDOWN,
                         text='You\'re not registered, please authenticate yourself using `/auth TOKEN` command!')

def unknown_command_handler(bot, update):
    """Unknown command handler."""
    logger.info('Unknown command from %s: %s' % (update.effective_user.id, update.effective_message.text))
    bot.send_message(chat_id=update.effective_message.chat_id, text='Sorry, I didn\'t understand that command. /help')

def unknown_text_handler(bot, update):
    """Unknown text handler."""
    logger.info('Unknown text from %s: %s' % (update.effective_user.id, update.effective_message.text))
    bot.send_message(chat_id=update.effective_message.chat_id, text='What are you telling me? /help')

def main():
    updater = Updater(token=TOKEN, workers=WORKERS)
    dispatcher = updater.dispatcher

    dispatcher.add_handler(CommandHandler('start', start_handler))
    dispatcher.add_handler(CommandHandler('help', help_handler))
    dispatcher.add_handler(CommandHandler('loveyou', love_handler))
    dispatcher.add_handler(CommandHandler('auth', auth_handler, pass_args=True))
    dispatcher.add_handler(CommandHandler('flag', flag_handler))
    dispatcher.add_handler(CallbackQueryHandler(game_callback, pattern=r'^game:\w+$'))
    dispatcher.add_handler(MessageHandler(Filters.command, unknown_command_handler))
    dispatcher.add_handler(MessageHandler(Filters.text, unknown_text_handler))

    updater.start_polling()
    updater.idle()

if __name__ == '__main__':
    main()