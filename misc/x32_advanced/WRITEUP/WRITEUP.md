# x32 Advanced Writeup

This task is a little bit more complicated and requires the use of almost every x32 instructions.

There can be multiple solutions but the following code performs the desired operation :

```
---- START ----
set A1, 0x1E
sub SP, 0x1E
in SP
set R4, SP      # stores a reference to the buffer
set R3, 0x0     # counter
<for_loop>      # label definition
set R2, R4
add R2, R3      
load R1, R2     # takes byte of the input starting from the top
xor R1, 0x58    # 0x58 = 'X'
store R2, R1    # overwrite the byte on the stack
add R3, 0x1     # move on to the next one
cmp R3, 0x1F    # reached the end ?
jl for_loop     # if not, loop
out R4          # print the result
---- END ----
```

ENOENT
