#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# Author: ENOENT

from assembler import Assembler
from emulator import Emulator
import os, sys

FLAG = "APRK{b4by_x32_t0_b3g1n}"
bytecode = bytes()

def runTest(input, expected):
    success = False
    emu = Emulator(bytecode, debug=False)
    emu.STDIN = input
    try:
        emu.run()
        out = emu.STDOUT
        if len(out) != 10:
            print("Your program must read 10 bytes from STDIN")
            success = False
        elif out == expected:
            success = True
        else:
            print("The input was : {}\nYour program outputted : {}\nExpected output was : {}".format(input, out, expected))
            success = False
        return success
    except Exception as inst:
        x, y = inst.args
        if x == "STDIN too small":
            print("Your program must read 10 bytes from STDIN")
        else:
            print("{} : {}".format(x,y))

def getUserCode():
    code = ""
    START = "---- START ----"
    END = "---- END ----"
    print("Please input your code.")
    print("Code must start with the following line :")
    print(START)
    print("And must end with the following line :")
    print(END)
    while sys.stdin.readline().strip() != START:
        pass
    line =  sys.stdin.readline()
    while line.strip() != END:
        code += line
        line =  sys.stdin.readline()
    return code

if __name__ == "__main__":
    asm = Assembler()
    try:
        bytecode = asm.compile(getUserCode())
        N = 10
        giveflag = True
        for i in range(N):
            data = os.urandom(10)
            r = runTest(data, data)
            if r:
                print("Test {}/{} : SUCCESS".format(i+1,N))
            else:
                print("Test {}/{} : FAILED".format(i+1,N))
                giveflag = False
                break
        if giveflag:
            print("Your code passed all the tests, here you go :")
            print(FLAG)

    except Exception as inst:
        x, y = inst.args
        print("{} : {}".format(x,y))
