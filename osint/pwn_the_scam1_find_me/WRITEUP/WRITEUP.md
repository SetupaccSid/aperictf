+++
title = "Pwn The Scam 1 - Find Me"
description = "Aperi'CTF 2019 - OSINT (100 pts)"
keywords = "OSINT, TOR, Directory Listing, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T13:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - OSINT (100 pts)"
toc = true
+++

Aperi'CTF 2019 - Pwn The Scam 1 - Find Me
============================================

### Challenge details

| Event                    | Challenge                | Category | Points | Solves      |
|--------------------------|--------------------------|----------|--------|-------------|
| Aperi'CTF 2019           | Pwn The Scam 1 - Find Me | OSINT    | 100    | ???         |

Un [site de scam Bitcoin](http://ylsspycahtqrv3u2.onion) a été découvert sur TOR. Vous avez été missionné pour en prendre le contrôle.

> *Pwn The Scam* est un challenge d'OSINT, il n'y a pas de vulnerabilité web à exploiter!
> Format de flag : *APRK{flag}*.

Investiguez sur ce que vous avez à disposition, chaque élément peut être déterminant.

### TL;DR
Browse URL with TOR then directory listing on `/free-btc/css` leads to `/free-btc/css/adm.css` finnaly guess the admin page `/free-btc/admin`, flag is in html comments.

### Methodology

#### TOR
The challenge URL is [http://ylsspycahtqrv3u2.onion](http://ylsspycahtqrv3u2.onion) which is a [TOR URL](https://fr.wikipedia.org/wiki/.onion). To browse the website, we'll use [tor-browser](https://www.torproject.org/download/) software.
The index [http://ylsspycahtqrv3u2.onion/free-btc/](http://ylsspycahtqrv3u2.onion/free-btc/).
<center>![home.jpeg](home.jpeg)</center>

#### Directory Listing

There is no form, the begin of the index.html source code is:

```html
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>BTC x100</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/landing-page.css" rel="stylesheet">

</head>
```

We got only one folder: `/css`, if we look at it we got a directory listing:

<center>![dirlist.jpeg](dirlist.jpeg)</center>

#### Guess / Fuzzing

On the directory listing, we can notice the file `adm.css`. We can suppose there is an admin page. We can either use `dirb` with `proxychains` to fuzz directory or guess the admin page with multiple try: [http://ylsspycahtqrv3u2.onion/free-btc/admin](http://ylsspycahtqrv3u2.onion/free-btc/admin)

<center>![form.jpeg](form.jpeg)</center>

#### HTML Comments

As said in the summary, there is no web vulnerability. Looking at the html comment we got the flag:

```html
<title>BTC x100 - Admin</title>

<link rel="stylesheet" href="../css/adm.css" />

<div class="login-page">
  <div class="form">
    <form class="login-form" method="POST">
      <input type="password" placeholder="password" name="p"/>
      <button>login</button>
    </form>
    <span></span>
  </div>
</div>

<!-- APRK{$4dm1n_P4g3$} -->
```

#### Flag

`APRK{$4dm1n_P4g3$}`

Challenge by [DrStache](https://twitter.com/drstache_) , WriteUp by [Zeecka](https://twitter.com/Zeecka_)
