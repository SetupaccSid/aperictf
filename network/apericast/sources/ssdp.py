#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import re

from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from netifaces import AF_INET, AF_INET6, AF_LINK, AF_PACKET, AF_BRIDGE
import netifaces as ni

LISTEN_ADDR = '0.0.0.0'
SERVER_NIC = 'eth0'
SERVER_ADDR = ni.ifaddresses(SERVER_NIC)[AF_INET][0]['addr']
SERVER_PORT = 8008
SSDP_GROUP = '239.255.255.250'
SSDP_PORT = 1900

UUID = 'deadbeef-cafe-babe-c0d3-cacafa11face'
TARGET_PATTERN = r'^ST:[\s]*(?P<service_type>(?:urn:dial-multiscreen-org:(?:(?:device)|(?:service)):dial:1)|(?:ssdp:all)|(?:upnp:rootdevice)|(?:uuid:%s))$' % UUID

RESPONSE = '''HTTP/1.1 200 OK
CACHE-CONTROL: max-age=1800
LOCATION: http://%s:%d/ssdp/device-desc.xml
OPT: "http://schemas.upnp.org/upnp/1/0/"; ns=01
01-NLS: %s
ST: urn:dial-multiscreen-org:service:dial:1
SERVER: Aperi'Cast by Creased
USN: uuid:%s::urn:dial-multiscreen-org:service:dial:1
BOOTID.UPNP.ORG: 0
EXT: 

'''.replace('\n', '\r\n') % (SERVER_ADDR, SERVER_PORT, UUID, UUID)

class MulticastPingPong(DatagramProtocol):
    def startProtocol(self):
        print(' * Running on %s:%d (Press CTRL+C to quit)' % (SSDP_GROUP, SSDP_PORT))
        # Set the TTL>1 so multicast will cross router hops:
        self.transport.setTTL(5)
        # Join a specific multicast group:
        self.transport.joinGroup(SSDP_GROUP)

    def datagramReceived(self, datagram, address):
        datagram = datagram.decode().replace('\r\n', '\n')
        print('=== Received from %s:%d ===\n\n%s=====================================\n' % (address[0], address[1], datagram))

        if ('M-SEARCH' in datagram) and (re.search(TARGET_PATTERN, datagram, re.MULTILINE)):  # Search Target matches the Chromecast device search.
            print(' * Sending Chromecast info!\n')
            # Rather than replying to the group multicast address, we send the
            # reply directly (unicast) to the originating port:
            print('=== Response to %s:%d ===\n\n%s=====================================\n' % (address[0], address[1], RESPONSE))
            self.transport.write(RESPONSE.encode(), address)

if __name__ == '__main__':
    reactor.listenMulticast(SSDP_PORT, MulticastPingPong(), listenMultiple=True)
    reactor.run()
