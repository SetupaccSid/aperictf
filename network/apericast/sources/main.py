#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Google Chromecast fake API based on https://rithvikvibhu.github.io/GHLocalApi/.

from flask import Flask, render_template, make_response, redirect, request
from netifaces import AF_INET, AF_INET6, AF_LINK, AF_PACKET, AF_BRIDGE
from uuid import uuid4
import netifaces as ni
import json

app = Flask(__name__)
SERVER_NIC = 'eth0'
SERVER_ADDR = ni.ifaddresses(SERVER_NIC)[AF_INET][0]['addr']
SERVER_PORT = '8008'
UUID = 'deadbeef-cafe-babe-c0d3-cacafa11face' # str(uuid4())
FLAG = 'APRK{B3w4r3_Wh47_Y0uR_D3v1c3s_4r3_54Y1n6_4b0u7_Y0U!}'
DEVICE_NAME = 'Dig deeper and RTFM!'

@app.route('/', methods=['GET'])
def home():
    html = render_template(
        'response.html',
        message='''Aperi'Cast, developed with <span style="color: #ba6060">❤</span> by Creased'''
    )
    response = make_response(html)
    response.headers['Content-Type'] = 'text/html'
    return response

@app.route('/setup/icon.png', methods=['GET'])
def icon():
    return redirect('https://www.aperikube.fr/img/logo.png')

@app.route('/setup/configured_networks', methods=['GET'])
def networks():
    html = render_template(
        'response.html',
        message='''An error occured while trying to get WiFi info, please check <a href="/setup/eureka_info">Eureka info</a> instead.'''
    )
    response = make_response(html)
    response.headers['Content-Type'] = 'text/html'
    return response

@app.route('/setup/eureka_info', methods=['GET'])
def eureka_info():
    data = {}
    info = {
        "audio": {
            "digital": False
        },
        "build_info": {
            "build_type": 2,
            "cast_build_revision": "1.39.151425",
            "cast_control_version": 1,
            "preview_channel_state": 0,
            "release_track": "stable-channel",
            "system_build_number": "151425"
        },
        "detail": {
            "icon_list": [
                {
                    "depth": 32,
                    "height": 55,
                    "mimetype": "image/png",
                    "url": "/setup/icon.png",
                    "width": 98
                }
            ],
            "locale": {
                "display_string": "français"
            },
            "timezone": {
                "display_string": "heure d’été d’Europe centrale (Paris)",
                "offset": 120
            }
        },
        "device_info": {
            "4k_blocked": 2,
            "capabilities": {
                "audio_hdr_supported": False,
                "audio_surround_mode_supported": False,
                "ble_supported": False,
                "cloudcast_supported": False,
                "display_supported": False,
                "fdr_supported": False,
                "hdmi_prefer_50hz_supported": False,
                "hdmi_prefer_high_fps_supported": False,
                "hotspot_supported": False,
                "https_setup_supported": False,
                "keep_hotspot_until_connected_supported": False,
                "multizone_supported": False,
                "opencast_supported": False,
                "preview_channel_supported": False,
                "reboot_supported": False,
                "setup_supported": False,
                "stats_supported": False,
                "system_sound_effects_supported": False,
                "wifi_auto_save_supported": False,
                "wifi_supported": True
            },
            "factory_country_code": "FR",
            "hotspot_bssid": "de:ad:be:ef:ca:fe",
            "manufacturer": "Google Inc.",
            "model_name": "Chromecast",
            "product_name": "chorizo",
            "public_key": "",
            "ssdp_udn": UUID,
            "uptime": 0
        },
        "name": DEVICE_NAME,
        "net": {
            "ethernet_connected": True,
            "ip_address": SERVER_ADDR,
            "online": True
        },
        "opt_in": {
            "audio_hdr": False,
            "audio_surround_mode": 0,
            "autoplay_on_signal": False,
            "cloud_ipc": False,
            "hdmi_prefer_50hz": False,
            "hdmi_prefer_high_fps": False,
            "managed_mode": False,
            "opencast": False,
            "preview_channel": False,
            "remote_ducking": False,
            "stats": False,
            "ui_flipped": False
        },
        "proxy": {
            "mode": "system"
        },
        "settings": {
            "closed_caption": {},
            "control_notifications": 1,
            "country_code": "FR",
            "locale": "fr",
            "network_standby": 0,
            "system_sound_effects": False,
            "time_format": 2,
            "timezone": "Europe/Paris",
            "wake_on_cast": 0
        },
        "setup": {
            "qr_ssid_suffix": "",
            "setup_state": 60,
            "ssid_suffix": "b",
            "stats": {
                "num_check_connectivity": 0,
                "num_connect_wifi": 0,
                "num_connected_wifi_not_saved": 0,
                "num_initial_eureka_info": 0,
                "num_obtain_ip":0
            },
            "tos_accepted": True
        },
        "version": 8,
        "wifi": {
            "bssid": "de:ad:be:ef:ca:fe",
            "has_changes": False,
            "noise_level": 0,
            "signal_level": 0,
            "ssid": FLAG,
            "wpa_configured": True,
            "wpa_id": 0,
            "wpa_state": 10
        }
    }
    default_info = ['name', 'build_info', 'detail']

    params = request.args.get('params')
    if params:
        for param in params.replace(' ', '').split(','):
            param = param.split('.')[0]
            if param in info:
                data[param] = info[param]
    else:
        for param in default_info:
            if param in info:
                data[param] = info[param]

    json_data = json.dumps(data)
    response = make_response(json_data)
    response.headers['Content-Type'] = 'application/hson'

    return response

@app.route('/setup/reboot', methods=['POST'])
def reboot():
    html = render_template(
        'response.html',
        message='''Rebooting... Sorry, it was a joke!'''
    )
    response = make_response(html)
    response.headers['Content-Type'] = 'text/html'
    return response

@app.route('/setup/set_eureka_info', methods=['POST'])
def set_eureka_info():
    html = render_template(
        'response.html',
        message='''You don't even know what you're actually doing, do you?'''
    )
    response = make_response(html)
    response.headers['Content-Type'] = 'text/html'
    return response

@app.route('/ssdp/device-desc.xml', methods=['GET'])
def device_desc():
    xml = render_template(
        "device-desc.xml",
        host=SERVER_ADDR,
        port=SERVER_PORT,
        uuid=UUID,
        name=DEVICE_NAME
    )

    response = make_response(xml)
    response.headers['Content-Type'] = 'application/xml'
    return response

if __name__ == '__main__':
    app.run(host=SERVER_ADDR, port=SERVER_PORT)
