# Not humans - 1

Abhf nibaf qépbhireg ha cbegnvy zranag iref ha nhger zbaqr!
Vairfgvthrm rg enzrarm har cerhir qr ivr cnenyyèyr cebiranag qr pr cbegnvy.
Oba pbhentr.

```bash
nc humans.aperictf.fr 33331
```

# Not humans - 2

Yr cbegnvy rfg znvagranag bhireg ! Vairfgvthrm rg enzrarm yr zbg qr cnffr nqzvavfgengrhe.

<u>**Note&nbsp;:**</u> Ce challenge intervient dans la continuité du challenge `Not humans - 1`.

```bash
nc humans.aperictf.fr 33331
```
