# Clipboard Jacking

La communauté AperiWallet a été victime d'une campagne d'attaques ciblées.
Ces attaques se traduisent par la diffusion d'un malware implémentant du ClipboardJacking, afin d'altérer les transactions des utilisateurs d'AperiWallet.
Un technicien a réussi à récupérer la charge malveillante envoyée aux membres de la communauté.
Votre but: récupérer les adresses de wallet crypto de l'attaquant.

Le flag est sous la forme APRK{SHA1(Adr1;Adr2)}

<u>**Note&nbsp;:**</u> 
- Les adresses sont à trier dans l'ordre alphabétique et en minuscules
- Veillez à ne pas insérer de doublons

<u>**Fichier&nbsp;:**</u> [ClipboardJacking.apk](files/ClipboardJacking.apk) - md5sum: a2e19fddae214175ae8247021e21d174
