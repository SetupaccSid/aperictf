const jni = require("./utils/jni_struct.js")

// First step : remove anti-root detection
Java.perform(function(){
    const MainActivity = Java.use("reverse.areizen.mygallery.MainActivity");
    MainActivity.k.implementation = function(){
        console.log("[+] Bypassed root check");
        return false;
    }
})



//Second_step :  Wait for the native library to load
library_name = "libnative-lib.so"
library_loaded = 0
Interceptor.attach(Module.findExportByName(null, 'android_dlopen_ext'),{
    onEnter: function(args){
        // first arg is the path to the library loaded
        library_path = Memory.readCString(args[0])

        if( library_path.includes(library_name)){
            console.log("[.] Loading library : " + library_path)
            library_loaded = 1
        }
    },
    onLeave: function(args){

        // if it's the library we want to hook, hooking it
        if(library_loaded ==  1){
            console.log("[+] Loaded")
            
            //Now we will hook the callback func
            hook_func()
            library_loaded = 0
        }
    }
})


function_name = "Java_reverse_areizen_mygallery_MainActivity_loadImages"
function hook_func(){
    // To get the list of exports
    Module.enumerateExportsSync(library_name).forEach(function(symbol){
        // console.log(symbol.name)
        if(symbol.name == function_name){
            console.log("[...] Hooking : " + library_name + " -> " + function_name + " at " + symbol.address)
            
            Interceptor.attach(symbol.address,{
                onEnter: function(args){
                   
                    jnienv_addr = Memory.readPointer(args[0])
                   
                    console.log("[+] Hooked successfully, JNIEnv base adress :" + jnienv_addr)

                    //jni.hook_all(jnienv_addr)
                    
                    /*
                    Interceptor.attach(jni.getJNIFunctionAdress(jnienv_addr,"FindClass"),{
                        onEnter: function(args){
                            console.log("env->FindClass(\"" + Memory.readCString(args[1]) + "\")")
                        }
                    })
                    */
                   /*
                   Interceptor.attach(jni.getJNIFunctionAdress(jnienv_addr,"SetByteArrayRegion"),{
                        onEnter: function(args){
                            var bytebuffer = Memory.readCString(args[4])
                            console.log("Memory region content : \"" + bytebuffer + "\"")
                        }
                    })
                    */
                    
                    Interceptor.attach(jni.getJNIFunctionAdress(jnienv_addr,"SetByteArrayRegion"),{
                        onEnter: function(args){
                            var bytebuffer = Memory.readCString(args[4])
                            console.log("env->SetByteArrayRegion(\"" + Memory.readCString(args[4]) + "\")")
                            
                            var buff = new Buffer(bytebuffer,'base64')
                            var base64decoded = buff.toString('utf8')
                            var regex = /APRK{.*}/g;
                            var found = base64decoded.match(regex);

                            console.log("[+++++] Flag is :" + found);
                        }
                    })
                    
                },
                onLeave: function(args){
                    // Prevent from displaying junk from other functions
                    Interceptor.detachAll()
                    console.log("[-] Detaching all interceptors")
                }
            })
        }
})
}
