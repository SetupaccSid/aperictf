# CAN you decode

## FR

Lors d'une mission de filature en voiture d'un membre des Magi'Kube,
celui-ci aurait agi bizarrement en effectuant une succession d'appel de phares avec sa Peugeot 407.
L'agence n'a pas pu identifier à qui s'adressait ce message, ni son contenu.
Un de vos collègues avait infiltré un ECU malveillant sur le bus de données CAN de la voiture.
Vous avez pu récupérer le dump des trames `0x046` du système de lumière en provenance du bus.
Votre tâche est donc de récupérer le contenu de ce message.

Le flag est de la forme `APRK XXX XXX ...`.

Il vous faudra le modifier en `APRK{XXX_XXX_...}`.

<u>**Fichiers&nbsp;:**</u>
- [BSI_BSM_opt_cmd.dump](files/BSI_BSM_opt_cmd.dump) - md5sum: f69cd012281ab2d5a584b72676a64bc2
- [OPT_CONTROL.txt](files/OPT_CONTROL.txt) - md5sum: cde2fb924ac38300740ecdb494f6e93e

## EN

During a car surveillance mission of a Magi'Kube member,
he would have acted strangely by making a series of headlamp calls with his Peugeot 407.
The agency could not identify to whom this message or its content was addressed.
One of your colleagues had infiltrated a malicious ECU on the car's CAN data bus.
You were able to retrieve the dump of the `0x046` frames of the light system from the bus.
Your task is therefore to retrieve the content of this message.

The flag is of the form `APRK XXX XXX XXX ...`.

You will need to change it to `APRK{XXX_XXX_XXX_...}`.

<u>**FIles&nbsp;:**</u>
- [BSI_BSM_opt_cmd.dump](files/BSI_BSM_opt_cmd.dump) - md5sum: f69cd012281ab2d5a584b72676a64bc2
- [OPT_CONTROL.txt](files/OPT_CONTROL.txt) - md5sum: cde2fb924ac38300740ecdb494f6e93e

