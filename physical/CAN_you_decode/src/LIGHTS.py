#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from random import randint

def gen_cmd_opt():
    can_period = 100
    dump_length = 2137
    total_seconds = (dump_length*can_period)/1000

    flag = ".- .--. .-. -.-/-.-- ----- ..-/-.-. ....- -./-... .-.. .---- -. -.-/-... .-.. .---- -. -.-/- ----- -----"
    f_flag, cpt = [], 0
    follow = 0
    for c in flag:
        t, pos = 0, 0
        if c == '-':
            t = 2
            p = 1
        elif c == '.':
            t = 0.5
            p = 1
        elif c == ' ':
            t = 2
            p = 0
        elif c == '/':
            t = 4
            p = 0
        if t:
            if p:
                cpt += follow
                f_flag.append([cpt, cpt+t])
                follow = 0.5
            else:
                follow = 0
            cpt += t
        else:
            print("ERROR")
            exit(1)

    A = L = 3
    D = E = K = O = P = 0

    B = [0, 3] # feux croisement
    C = [0, 1] # feux route
    C_cycle = [[17, 102]]
    J = [0, 1] # cmd occultant croisement / route

    M = [0, 1] # feux stop
    M_cycle = [[4, 7], [13, 15], [52, 60], [65, 67], [83, 87], [89, 93], [153, 160], [169, 173], [197, 204]]

    N = [0, 1] # feux recul
    N_cycle = [[93, 102]]

    H = [0, 1] # cligno av droit
    Q = [0, 1] # cligno ar droit
    HQ_cycle = [[156, 166]]
    I = [0, 1] # cligno av gauche
    R = [0, 1] # cligno ar gauche
    IR_cycle = [[9, 19]]

    F = (0, 255) # rapport cyclique
    G = (0, 255) # rapport cyclique

    f = cpt = 0
    for i in range(dump_length):
        seconds = (i*total_seconds)/dump_length

        for j in f_flag:
            B = 3
            C = J = 0
            if j[0] < seconds <= j[1]:
                B = 0
                C = J = 1
                break

        for j in M_cycle:
            M = 0
            if j[0] < seconds <= j[1]:
                M = 1
                break

        for j in N_cycle:
            N = 0
            if j[0] < seconds <= j[1]:
                N = 1
                break

        for j in HQ_cycle:
            H = Q = 0
            if j[0] < seconds <= j[1]:
                H = Q = 1
                break
        for j in IR_cycle:
            I = R = 0
            if j[0] < seconds <= j[1]:
                I = R = 1
                break

        F = randint(0, 255)
        G = randint(0, 255)

        print(A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R)


