# Write up

1. Find the location of the high beam headlights in the frame.

```raw
------------------------------------------------------------------------
OPT_CONTROL   |                     FR                     |    RE     |
------------------------------------------------------------------------
FR_LANT_S     | xx.. ....  .... ....  .... ....  .... .... | .... .... |
CODE_BEAM_S   | ..xx ....  .... ....  .... ....  .... .... | .... .... |
HIGH_BEAM_S   | .... x...  .... ....  .... ....  .... .... | .... .... |
FR_FOG_S      | .... .xx.  .... ....  .... ....  .... .... | .... .... |
Free1         | .... ...x  .... ....  .... ....  .... .... | .... .... |
RTC_PWM_DC    | .... ....  xxxx xxxx  .... ....  .... .... | .... .... |
LTC_PWM_DC    | .... ....  .... ....  xxxx xxxx  .... .... | .... .... |
R_BLINKING    | .... ....  .... ....  .... ....  x... .... | .... .... |
L_BLINKING    | .... ....  .... ....  .... ....  .x.. .... | .... .... |
BLACKOUT      | .... ....  .... ....  .... ....  ..x. .... | .... .... |
Free5         | .... ....  .... ....  .... ....  ...x xxxx | .... .... |
RE_LANT_S     | .... ....  .... ....  .... ....  .... .... | xx.. .... |
STOP_S        | .... ....  .... ....  .... ....  .... .... | ..x. .... |
REVERSE_S     | .... ....  .... ....  .... ....  .... .... | ...x .... |
RE_FOG_S      | .... ....  .... ....  .... ....  .... .... | .... x... |
TRAIL_FOG_S   | .... ....  .... ....  .... ....  .... .... | .... .x.. |
R_BLINK_S     | .... ....  .... ....  .... ....  .... .... | .... ..x. |
L_BLINK_S     | .... ....  .... ....  .... ....  .... .... | .... ...x |
------------------------------------------------------------------------
```

- High beam position : *HIGH_BEAM_S*
- Dipped-beam position : *CODE_BEAM_S*
  - (You can also solve with this one since it corresponds to the opposite of the high beam in this dump).
- Occultation between high and dipped-beam headlamps : *BLACKOUT*
  - (You can also solve with this one since it corresponds to the switch between the high beam and low beam headlights).

2. Dump all *HIGH_BEAM_S* bits.

3. Identify morse code

In seconds.

| short | long |
|-------|------|
| 0.5   | 2    |

Blank intervals :

| Between morse letters | Between letters | Between words |
|-----------------------|-----------------|---------------|
| 0.5                   | 2               | 4             |

4. Decode morse
