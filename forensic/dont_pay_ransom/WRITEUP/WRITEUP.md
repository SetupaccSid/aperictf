+++
title = "Don't pay ransom"
description = "Aperi'CTF 2019 - Forensic (175 pts)"
keywords = "Forensic, ZIP, Cracking, pkcrack, know-plaintext, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T13:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - Forensic (175 pts)"
toc = true
+++

Aperi'CTF 2019 - Don't pay ransom
============================================

### Challenge details

| Event                    | Challenge        | Category | Points | Solves      |
|--------------------------|------------------|----------|--------|-------------|
| Aperi'CTF 2019           | Don't pay ransom | Forensic | 175    | ???         |

### Description

Un étudiant a été victime d'une attaque par ransomware et a perdu l'accès à son secret.

Venez-lui en aide et retrouvez son secret.

Challenge: [Aziram.github.io.crypted](files/Aziram.github.io.crypted) - md5sum: 715c6d9a185f7d6054b5dd7366bd2502

### TL;DR

The provided file is an encrypted ZIP file, a known-plaintext attack can be carried out with only one known file to decrypt the ZIP file.

### Methodology

We’re given an [Aziram.github.io.crypt](files/Aziram.github.io.crypt) file.

Task description:

 > A student has been affected by a ransomware attack and has lost his secret.
 >
 > Help him retrieve his secret.


#### File analysis

The file is suffixed with the `.crypt` string, so it's probably an encrypted file. Let's analyze it using `binwalk`:

```bash
$ binwalk Aziram.github.io.crypt
DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             Zip archive data, at least v1.0 to extract, name: Aziram.github.io/
...
2233385       0x221429        End of Zip archive
```

The file seems to be a ZIP file:

```bash
$ zipinfo Aziram.github.io.crypt
...
Central directory entry #203:
---------------------------

  There are an extra 16 bytes preceding this file.

  Aziram.github.io/secret.txt

  offset of local header from start of archive:   2209471
                                                  (000000000021B6BFh) bytes
  file system or operating system of origin:      Unix
  version of encoding software:                   3.0
  minimum file system compatibility required:     MS-DOS, OS/2 or NT FAT
  minimum software version required to extract:   1.0
  compression method:                             none (stored)
  file security status:                           encrypted
  extended local header:                          yes
  file last modified on (DOS date/time):          2019 May 9 15:31:24
  file last modified on (UT extra field modtime): 2019 May 9 15:31:23 local
  file last modified on (UT extra field modtime): 2019 May 9 13:31:23 UTC
  32-bit CRC value (hex):                         8852645c
  compressed size:                                35 bytes
  uncompressed size:                              23 bytes
  length of filename:                             27 characters
  length of extra field:                          24 bytes
  length of file comment:                         0 characters
  disk number on which file begins:               disk 1
  apparent file type:                             text
  Unix file attributes (100644 octal):            -rw-r--r--
  MS-DOS file attributes (00 hex):                none

  The central-directory extra field contains:
  - A subfield with ID 0x5455 (universal time) and 5 data bytes.
    The local extra field has UTC/GMT modification/access times.
  - A subfield with ID 0x7875 (Unix UID/GID (any size)) and 11 data bytes:
    01 04 e8 03 00 00 04 e8 03 00 00.

  There is no file comment.
```

The ZIP file contains a GitHub repo and a `secret.txt` thus, according to the challenge description, contains the lost secret of the student.

Looking at the [original repo](https://github.com/Aziram/Aziram.github.io), we can assume that we can decrypt the ZIP using a known-plaintext attack.

Let's fire up the `pkcrack` tool!

#### Known-plaintext

First, we need to pick a large unencrypted file from the original repo (note: the larger the file, the more likely the attack is to work). Here, we will use the [bg01.jpg](https://github.com/Aziram/Aziram.github.io/blob/master/images/bg01.jpg) file.

It's important to ensure that the file contained in the ZIP file is the same as the one we have downloaded.

Here, we can refer to the date on which the ZIP file was created and the git repo commits:

```bash
$ zipinfo Aziram.github.io.crypt | grep "bg01.jpg"
-rw-r--r--  3.0 unx   190429 BX defN 19-May-09 15:00 Aziram.github.io/images/bg01.jpg
```

```bash
$ git clone https://github.com/Aziram/Aziram.github.io/
$ cd ./Aziram.github.io/
$ git log ./images/bg01.jpg
commit c568df959725c8b6bff5c15f3d190271a57d9aef
Author: Aziram <damien_lagarde@orange.fr>
Date:   Wed Apr 3 23:18:15 2019 +0200

    Init commit
```

The file has not been modified since the ZIP creation, we can then assume that the file is the same in the ZIP file. We can now decrypt the ZIP file:

```bash
$ cd ./images/
$ zip img.zip bg01.jpg
$ mv img.zip ../../
$ cd ../../
$ pkcrack -C Aziram.github.io.crypt -c Aziram.github.io/images/bg01.jpg -P img.zip -p bg01.jpg -d decrypted.zip
```

#### Flag

The final flag is `APRK{KN0wn_Cl34r_FTW!}`

*Happy Hacking!*

*Creased*
