#!/bin/bash
set -e
git clone https://github.com/Aziram/Aziram.github.io/
cd Aziram.github.io/images/
zip img.zip bg01.jpg
mv img.zip ../../
cd ../../
pkcrack -C ../files/Aziram.github.io.crypt -c Aziram.github.io/images/bg01.jpg -P img.zip -p bg01.jpg -d decrypted.zip
unzip decrypted.zip
cat Aziram.github.io/secret.txt
