+++
title = "OP'Rikube"
description = "Aperi'CTF 2019 - Forensic (50 pts)"
keywords = "Forensic, Keepass, Cracking, Bruteforce, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T13:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - Forensic (50 pts)"
toc = true
+++

Aperi'CTF 2019 - OP'Rikube
============================================

### Challenge details

| Event                    | Challenge | Category | Points | Solves      |
|--------------------------|-----------|----------|--------|-------------|
| Aperi'CTF 2019           | OP'Rikube | Forensic | 50    | ???         |

### Description

Nos services de renseignements se sont infiltrés sur le canal Telegram d'un groupe hacktiviste suspecté d'avoir participé à l'opération #OP'Rikube.

De nombreux messages ont été supprimés sur ce canal suite à l'exécution de l'opération. Cependant, un fichier a pu être récupéré.

Selon nos sources, de nombreux hackers ont participé à cette opération, l'un d'entre eux serait un membre du groupe Aperi'Kube.

Analysez ce fichier et retrouvez des informations sur ce hacker.

Challenge: [oprikube.kdbx](files/oprikube.kdbx) - md5sum: 1eabca64cb4da20aa18f6a0e4e169987

### TL;DR

Crack the password of the KDBX file, export keepass data into a CSV file, search for `APRK`, get the flag.

### Methodology

#### Password cracking

We’re given an [oprikube.kdbx](files/oprikube.kdbx) file.

Task description:

 > Our intelligence services infiltrated the Telegram channel of an hacktivist group alleged to have been involved in the #OP'Rikube operation.
 >
 > Many messages have been deleted on this channel following the operation. However, a file has been saved.
 >
 > According to our sources, many hackers were involved in the operation, one of them rleportedly was a member of the Aperi'Kube group.
 >
 > Analyze this file and find information about this hacker.

Since, we're given a KDBX file, let's try to crack its password using a wordlist!

Generate a john-the-ripper compatible hash:

```bash
keepass2john files/oprikube.kdbx | tee hash.txt
```

```
oprikube:$keepass$*2*60000*0*fd34bf8f4e1c2150e5e6836ee4e56f32601e60d5abc167e4ac809b992f18f7c6*e7992bb735608c3911d25008cf5327209c980028b86d0a9ae3fa559178e3bc64*c858d2275c618140a574d2f1fae8d83e*85d19effb6e37dc69e5e93e781573994751c47ab94f85085f89038de4b97ecdc*01255337deffa72aa90734c450e33c30379048c793ef8f9ec9aaa1d4b7208826
```

Crack the password:

```bash
john --format=KeePass --fork=20 --wordlist=rockyou.txt hash.txt
john --show hash.txt
```

```
oprikube:password
```

Woot! Let's open it using KeePass2:

![keepass entries](img/keepass_entries.png)

Looking at the entries, the file is composed of several users, the only item that allows us to identify them is their password (i. e., `TEAM{sha1(user_id)}`).

The user we're looking for is a member of the Aperi'Kube team (`APRK`), let's look for this user!

#### User lookup

First, we need to export the user database using the `File > Export > KeePass CSV (1.x)` feature then grep it for the `APRK symbol`:

```bash
grep "APRK" oprikube.csv
```

Result:

```
"user758","user758","APRK{82a50612a57ad5c00b0df9bafbcd379d25c6fbda}","",""
```

The user we're looking for is the `user758`!

The final flag is `APRK{82a50612a57ad5c00b0df9bafbcd379d25c6fbda!}`

*Happy Hacking!*

*Creased*
