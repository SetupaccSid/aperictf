# Double Trouble

Vous avez été mandaté par la société ENO.corp pour analyser [le logiciel de chiffrement des backups](crypt.py) développé par un de leurs meilleurs stagiaires.

Votre mission est de trouver les vulnérabilités cryptographiques présentes dans son code et les exploiter pour déchiffrer [ce fichier de backup](flag.txt.zip.enc).

<u>**Fichiers&nbsp;:**</u>
- [crypt.py](files/crypt.py) - md5sum: 7349cc9656477d97de63a406bf743320
- [flag.txt.zip.enc](files/flag.txt.zip.enc) - md5sum: 282e839cd962fc1ed416f8d4578b87d4
