# Real Spy Agent

Vous êtes un agent infiltré dont la mission est d'espionner les faits et gestes du grand patron d'ENO.corp.
Vous savez de source sûre que cette société utilise un système cryptographique biaisé, basé sur le chiffrement RSA, pour échanger des messages secrets entre ses membres.
Vous avez réussi à intercepter un de ces messages en provenance du patron, votre tâche est le déchiffrer au plus vite !

<u>**Fichiers&nbsp;:**</u> [CEO_pb_key](files/CEO_pb_key.txt) - md5sum: dc3bc6c21a493d7533a18e67f50ff01f
[your_pvt_key](files/your_pvt_key.txt) - md5sum: 7d26deb3ffbd4edd09832e231bbb827b
[message](files/message.enc) - md5sum: 5a2e6d32d6cdef50af8d61b63707bc48