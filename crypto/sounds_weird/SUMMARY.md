# Sounds Weird

Afin de protéger les fichiers de l'entreprise, un développeur a proposé une solution de chiffrement à sa DSI.
Votre DSI vous a par conséquent demandé de tester la résistance de cette solution en vous fournissant un fichier chiffré.

<u>**Fichier&nbsp;:**</u> [flag.ogg.crypt](files/flag.ogg.crypt) - md5sum: 0dbd09dc7546998c7d3fe230486be377
