#### Decrypt

# Headers

b = "\x4F\x67\x67\x53\x00\x02\x00\x00\x00\x00\x00\x00\x00\x00"  # Default file
a = "\x0e\x17\x54\x21\x31\x69\x75\x62\x65\x66\x6c\x34\x67\x21"  # Cipher

key = ""
for x in zip(a,b):
    key += chr(ord(x[0])^ord(x[1]))

with open("flag.ogg.crypt", 'rb') as f:
    file1 = f.read()

file2 = bytearray(len(file1))

for i in range(len(file1)):
    file2[i] = ord(file1[i]) ^ ord(key[i%len(key)])

# Write the XORd bytes to the output file
with open("flag_decrypt.ogg", 'wb') as f:
    f.write(file2)
