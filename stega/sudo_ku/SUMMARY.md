# Sudo ku

You wanna play a *game* ? Then follow the rules.

<u>**Fichiers&nbsp;:**</u>
- [TheGame.png](files/TheGame.png) - md5sum: 61d3fdc5f9161d9e2720bc3d6511b9e1
- [YouLoose.jpg](files/YouLoose.jpg) - md5sum: 5d4ccd32191d7dd6bb5823bab4e3b528
<br>
<u>**Ressources complémentaires&nbsp;:**</u>
- [Steganography using Sudoku Puzzle.pdf](files/Steganography using Sudoku Puzzle.pdf) - md5sum: d9422e48a7931bc073ac1a35703176ea
