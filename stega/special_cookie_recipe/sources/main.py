#!/usr/bin/env python2
# -*- coding:utf-8 -*-
# ./main.py clean; ./main.py setup; ./main.py test

import sys
import binascii
import string
from os import popen

FLAG = "APRK{ciboulette}"


def usage():
    print "Usage: {:s} setup | test | clean".format(sys.argv[0])
    exit(1)


def bits2string(b=None):
    return ''.join([chr(int(x, 2)) for x in b])


def setup():
    with open("cookies.txt", "r") as f:
        recipe = f.read()
    flag_bin = str(bin(int(binascii.hexlify(FLAG), 16))).split('b')[1]
    if (len(flag_bin) % 2) != 0:
        flag_bin = "0" + flag_bin
    flag_bin = flag_bin.ljust(len(recipe), "0")
    print "{} -> {}".format(FLAG, flag_bin)

    encoded = str()
    index_flag = 0
    for c in recipe:
        if c in string.letters:
            if flag_bin[index_flag] == "0":
                encoded += string.lower(c)
            elif flag_bin[index_flag] == "1":
                encoded += string.upper(c)
            index_flag += 1
        else:
            encoded += c

    with open("chall_cookies.txt", "w") as f:
        f.write(encoded)

    print
    print "cat chall_cookies.txt"
    print popen("cat chall_cookies.txt").read()

def exploit():
    with open("chall_cookies.txt", "r") as f:
        encoded = f.read()

    flag_bin = str()
    for e in encoded:
        if e in string.lowercase:
            flag_bin += "0"
        if e in string.uppercase:
            flag_bin += "1"
    print flag_bin
    chars = map(''.join, zip(*[iter(flag_bin)]*8))
    decoded = str()
    for c in chars:
        decoded += chr(int(c, 2))
    print decoded
    if FLAG in decoded:
        print "Exploit ok ! :-)"
    else:
        print "Exploit ko ! :-("



if len(sys.argv) != 2:
    usage()

action = sys.argv[1]

if action == "setup":
    setup()
elif action == "test":
    exploit()
elif action == "clean":
    popen("rm chall_cookies.txt")
else:
    usage()
