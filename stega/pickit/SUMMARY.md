# Pickit

Vous être analyste au bureau d'analyse et d'imagerie de l'entreprise Pegasus.

Suite à une fuite de données massive chez un gros conglomérat, vous avez été mandaté
en tant qu'expert judiciaire pour analyser une image trouvée sur la clé USB d'un des suspects.

Votre collègue, passionné de physique théoricienne vous a laissé un message&nbsp;:

    Cette image me semble différente de l'originale, peut-être plus claire et plus petite...
    Le gars est joueur, s'il a caché des messages, il a probablement utilisé différentes techniques !

Retrouvez les messages cachés dans cette image&nbsp;!

<u>**Fichier&nbsp;:**</u> [pickit.png](files/pickit.png) - md5sum: b465ec3e0cd951ffeb8d55d89f1c2f80
