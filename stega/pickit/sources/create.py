#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Author: Baptiste MOINE <contact@bmoine.fr>.

import argparse
import traceback
import os
import pyqrcode

from libs.shared import (calc_crc, change_height, logger)
from libs.palette import Palette
from libs.png import (PNG, PNG_MAGIC_NUMBER, PNG_MAGIC_NUMBER_LENGTH)
from mmap import (ACCESS_READ, mmap)
from PIL import (Image, ImageOps, ImagePalette)

def palette_image(img, bits=8, colors=8):
    """Create paletted image."""
    img = img.convert('RGB')  # convert the image to RGB
    img = ImageOps.posterize(img, bits)  # clamp the color channels to n-bit values
    img = img.convert('P', palette=Image.ADAPTIVE, colors=colors, dither=Image.FLOYDSTEINBERG)  # convert the image to paletted image and keep n slots for palette

    return img

def str_to_palette(str_):
    """Convert a string to a palette."""
    list_ = []
    for c in map(ord, str_):
        list_ += [c]
        list_ += [0]

    palette = Palette(list_)

    return palette

def apply_palette(img, img_palette):
    """Apply a palette to a image."""
    img = img.convert('P')
    img.putpalette(img_palette.palette)

    return img

def prepare_qr_code(file_, size, data):
    """Ajust the QRCode size to the image."""
    (img_w, img_h) = size

    qr = pyqrcode.create(data, error='H', version=8, mode='binary', encoding='utf-8')
    qr.png(file_, scale=6, module_color=[0, 0, 0, 255], background=None)

    qrcode = Image.open(file_)
    (qr_w, qr_h) = qrcode.size

    qr_img = Image.new('RGB', (img_w, qr_h), (255, 255, 255))
    qr_img.paste(qrcode, ((img_w - qr_w) // 2, 0))

    return qr_img

def append_img(img, qrcode, black=0, white=255):
    """Add the QRCode into to the image."""
    (img_w, img_h) = img.size
    (qr_w, qr_h) = qrcode.size
    pxs = qrcode.getdata()
    new_data = []
    for color in pxs:
        if color == (0, 0, 0):
            color = (black, black, black)
        else:
            color = (white, white, white)
        new_data.append(color)
    qrcode.putdata(new_data)
    img = add_pixel_line(img, qr_h)
    img.paste(qrcode, (0, img_h))

    return img

def add_pixel_line(img, px_line=1):
    """Add n pixel line to the bottom of the image."""
    (width, height) = img.size
    new_img = Image.new('RGB', (width, height + px_line), color=(255, 255, 255))
    new_img.paste(img)

    return new_img

def lsb_encode(img, flag):
    """Encode a message into image using LSB."""
    (img_w, img_h) = img.size

    flag = ''.join('{:08b}'.format(ord(c)) for c in flag)
    newdata = []

    for i in range(len(flag)):
        x = i % img_w
        y = i // img_w

        img_color = img.getpixel((x, y))
        img_color = (img_color - img_color % 2) + int(flag[i])
        newdata.append(img_color)

    img.putdata(newdata)

    return img

def add_ztxt(img, data):
    """Add extra zTXt chunk to the image."""
    with open(img, 'rb') as fd:
        # Load the picture to memory.
        mm = mmap(fd.fileno(), 0, access=ACCESS_READ)

        # Skip file header.
        mm.seek(PNG_MAGIC_NUMBER_LENGTH, os.SEEK_SET)

        # Create the main IMAGE object.
        png = PNG(mm)

        # Hide data inside the zTXt chunk.
        png.hideData(data)

        # Free memory mapping.
        mm.close()

    # Write a new PNG
    with open(img, 'wb') as fd:
        fd.write(PNG_MAGIC_NUMBER)
        fd.write(png.getBytes())

def parse_args():
    """Arguments parsing."""
    parser = argparse.ArgumentParser()

    parser.add_argument('-i', '--input',
                        type=str,
                        default='data/input.png',
                        help='path to the source image file')

    parser.add_argument('-o', '--output',
                        type=str,
                        default='data/output.png',
                        help='path to the destination image file')

    parser.add_argument('-q', '--qrcode',
                        type=str,
                        default='data/qrcode.png',
                        help='path to the qrcode image file')

    args = parser.parse_args()

    return args

def main():
    """Load image and generate a candidate challenge file."""
    try:
        # Arguments parsing.
        args = parse_args()

        # Print configuration.
        logger.info('Input file: {0}'.format(args.input))
        logger.info('Output image: {0}'.format(args.output))
        logger.info('QRCode image: {0}'.format(args.qrcode))

        # Prepare flags.
        flag1='p1:APRK{l00k_3v3rywh3r3!!}'       # QRCode
        flag2='p2:APRK{d16_d33p3r_n_d33p3r...}'  # LSB
        flag3='p3:APRK{n1c3_c47ch_c4rry_0n!!}'   # zTXT
        flag4='p4:APRK{c0n6r47z_d4rk_h4xx0r!!}'  # Palette

        flag_palette = str_to_palette(flag4)

        # Load images.
        img = Image.open(args.input)
        qrcode = Image.open(args.qrcode)
        (img_w, img_h) = img.size
        (qr_w, qr_h) = qrcode.size

        # Prepare the qrcode.
        qrcode = prepare_qr_code(args.qrcode, img.size, flag1)

        # Add the QRCode under the image.
        img = append_img(img, qrcode)

        # Prepare the image.
        img = img.convert('L')
        img = ImageOps.invert(img)

        # Create paletted image.
        flag_color_count = (len(flag4)*2//3) + 1
        img = palette_image(img=img, bits=8, colors=(256-(flag_color_count)))

        # Get palette of the image.
        img_palette = Palette(img.getpalette())

        logger.debug('Original palette: {0}'.format(img_palette))

        # Rotate and compress the palette.
        img_palette.pop_color(flag_color_count)
        img_palette.rotate()

        # Hide the flag into the palette.
        img_palette.merge(flag_palette)
        img = apply_palette(img, Palette(img_palette.palette))

        logger.debug('New palette: {0}'.format(Palette(img.getpalette())))

        # Add LSB message.
        img = lsb_encode(img, flag2)

        # Save final image.
        img.save(args.output, format='PNG')

        # Reduce the size from IHDR
        change_height(args.output, img_h)

        # Add flag in a zTXt chunk.
        add_ztxt(args.output, flag3)
    except (ValueError, TypeError, OSError, IOError):
        logger.error(traceback.format_exc())

## Runtime processor.
if __name__ == '__main__':
    main()
