# Cat's Eyes writeup

We’re given a [cats_eyes.png](files/cats_eyes.png) file.

Task description:

 > A scientific research laboratory has been attacked and some documents have leaked.
 >
 > According to media reports, this laboratory studies, among other things, visual stimuli and response persistence of cat retinal ganglion.
 >
 > You've found one of these documents, analyze it and learn more about their research!

## File analysis

First things first, let's quickly analyze the PNG file:


```bash
file ./cats_eyes.png
binwalk ./cats_eyes.png
```

    ./cats_eyes.png: PNG image data, 640 x 320, 8-bit/color RGBA, non-interlaced

    DECIMAL       HEXADECIMAL     DESCRIPTION
    --------------------------------------------------------------------------------
    0             0x0             PNG image, 640 x 320, 8-bit/color RGBA, non-interlaced
    99            0x63            Zlib compressed data, best compression
    328355        0x502A3         Zlib compressed data, best compression
    334692        0x51B64         Zlib compressed data, best compression

Nothing really interesting here... Let's open the file in our image reader, I personally use [Mirage](http://mirageiv.sourceforge.net/):

![mirage](img/mirage.png)

Again, nothing very interesting, let's check the PNG chunks using [TweakPNG](https://github.com/jsummers/tweakpng):

![tweakpng](img/tweakpng.png)

Okay, there's definitely something here!

The PNG image is animated and contains:

 * A first frame that is displayed for 5 seconds
 * A second frame that is displayed for 200 milliseconds: we understand that the subject of animation is to verify retinal persistence
 * A third frame that is displayed for 5 seconds

Let's change the image so that we don't display the first and third image, but only the second one:

![tweakpng_solved](img/tweakpng_solved.png)

Result:

![solved](img/solved.png)

The final flag is `APRK{4N1M4710N_C0N7R0L_CHUNK}`

*Happy Hacking!*

*Creased*
